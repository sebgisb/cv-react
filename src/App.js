import React, { useState, useEffect } from 'react'
import Desktop from './components/desktop/Desktop';
import Toolbar from './components/toolbar/Toolbar';
import './styles/app.scss'
import Params from './components/apps/params/Params';
import Curriculum from './components/apps/curriculum/Curriculum';
import Minesweeper from './components/apps/minesweeper/Minesweeper';
import Quizz from './components/apps/quizz/Quizz';
import Git from './components/apps/git/Git';
import Netflix from './components/apps/netflix/Netflix';
import Calendar from './components/apps/calendar/Calendar';

const GlobalContext = React.createContext()
function App() {

     const [isParamsOpen, setIsParamsOpen] = useState(false)
     const [isParamsFullScreen, setIsParamsFullScreen] = useState(false)
     const [isParamsMinimized, setIsParamsMinimized] = useState(false)
     const [paramsBackground, setParamsBackground] = useState('windows.jpg')
     const [paramsSizeMultiplier, setParamsSizeMultiplier] = useState(1)
     const ParamsIcone = 'settings.png'
     const baseFontSize = 10;

     const [isCvOpen, setIsCvOpen] = useState(false)
     const [isCvFullScreen, setIsCvFullScreen] = useState(false)
     const [isCvMinimized, setIsCvMinimized] = useState(false)
     const CvIcone = 'cv.png'

     const [isMinesweeperOpen, setIsMinesweeperOpen] = useState(false)
     const [isMinesweeperFullScreen, setIsMinesweeperFullScreen] = useState(false)
     const [isMinesweeperMinimized, setIsMinesweeperMinimized] = useState(false)
     const MinesweeperIcone = 'mine.png'

     const [isQuizzOpen, setIsQuizzOpen] = useState(false)
     const [isQuizzFullScreen, setIsQuizzFullScreen] = useState(false)
     const [isQuizzMinimized, setIsQuizzMinimized] = useState(false)
     const QuizzIcone = 'quizz.png'

     const [isGitOpen, setIsGitOpen] = useState(false)
     const [isGitFullScreen, setIsGitFullScreen] = useState(false)
     const [isGitMinimized, setIsGitMinimized] = useState(false)
     const GitIcone = 'git.png'

     const [isNetflixOpen, setIsNetflixOpen] = useState(false)
     const [isNetflixFullScreen, setIsNetflixFullScreen] = useState(false)
     const [isNetflixMinimized, setIsNetflixMinimized] = useState(false)
     const NetflixIcone = 'netflix.png'

     const [isCalendarOpen, setIsCalendarOpen] = useState(false)
     const [isCalendarFullScreen, setIsCalendarFullScreen] = useState(false)
     const [isCalendarMinimized, setIsCalendarMinimized] = useState(false)
     const CalendarIcone = 'calendar.png'

     const appsValues = {
          'params': {
               title: 'Paramètres',
               icone: ParamsIcone,
               isOpen: isParamsOpen,
               setIsOpen: setIsParamsOpen,
               isMinimized: isParamsMinimized,
               setIsMinimized: setIsParamsMinimized,
               isFullScreen: isParamsFullScreen,
               setIsFullScreen: setIsParamsFullScreen,
               sizeMultiplier: paramsSizeMultiplier,
               setSizeMultiplier: setParamsSizeMultiplier,
               background: paramsBackground,
               setBackground: setParamsBackground,
          },
          'cv': {
               title: 'Cv',
               icone: CvIcone,
               isOpen: isCvOpen,
               setIsOpen: setIsCvOpen,
               isMinimized: isCvMinimized,
               setIsMinimized: setIsCvMinimized,
               isFullScreen: isCvFullScreen,
               setIsFullScreen: setIsCvFullScreen
          },
          'minesweeper': {
               title: 'Démineur',
               icone: MinesweeperIcone,
               isOpen: isMinesweeperOpen,
               setIsOpen: setIsMinesweeperOpen,
               isMinimized: isMinesweeperMinimized,
               setIsMinimized: setIsMinesweeperMinimized,
               isFullScreen: isMinesweeperFullScreen,
               setIsFullScreen: setIsMinesweeperFullScreen
          },
          'quizz': {
               title: 'Quizz',
               icone: QuizzIcone,
               isOpen: isQuizzOpen,
               setIsOpen: setIsQuizzOpen,
               isMinimized: isQuizzMinimized,
               setIsMinimized: setIsQuizzMinimized,
               isFullScreen: isQuizzFullScreen,
               setIsFullScreen: setIsQuizzFullScreen
          },
          'git': {
               title: 'Gitlab',
               icone: GitIcone,
               isOpen: isGitOpen,
               setIsOpen: setIsGitOpen,
               isMinimized: isGitMinimized,
               setIsMinimized: setIsGitMinimized,
               isFullScreen: isGitFullScreen,
               setIsFullScreen: setIsGitFullScreen
          },
          'netflix': {
               title: 'Netflix',
               icone: NetflixIcone,
               isOpen: isNetflixOpen,
               setIsOpen: setIsNetflixOpen,
               isMinimized: isNetflixMinimized,
               setIsMinimized: setIsNetflixMinimized,
               isFullScreen: isNetflixFullScreen,
               setIsFullScreen: setIsNetflixFullScreen
          },
          // 'calendar': {
          //      title: 'Planning',
          //      icone: CalendarIcone,
          //      isOpen: isCalendarOpen,
          //      setIsOpen: setIsCalendarOpen,
          //      isMinimized: isCalendarMinimized,
          //      setIsMinimized: setIsCalendarMinimized,
          //      isFullScreen: isCalendarFullScreen,
          //      setIsFullScreen: setIsCalendarFullScreen
          // }
     }

     useEffect(() => {
          document.querySelector('html').style.fontSize = baseFontSize * paramsSizeMultiplier + 'px';
     }, [paramsSizeMultiplier])



     return (
          <div className="app">
               <Desktop appsValues={appsValues}>
                    {isParamsOpen && <Params values={appsValues.params} />}
                    {isCvOpen && <Curriculum values={appsValues.cv} />}
                    {isMinesweeperOpen && <Minesweeper values={appsValues.minesweeper} />}
                    {isQuizzOpen && <Quizz values={appsValues.quizz} />}
                    {isGitOpen && <Git values={appsValues.git} />}
                    {isNetflixOpen && <Netflix values={appsValues.netflix} />}
                    {/* {isCalendarOpen && <Calendar values={appsValues.calendar} />} */}
               </Desktop>

               <Toolbar appsValues={appsValues} />
          </div>

     );
}

export default App;
