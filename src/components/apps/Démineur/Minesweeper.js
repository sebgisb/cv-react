import React, { useEffect, useState, useRef } from 'react'
import Window from '../components/window/Window'
import Grid from './components/Grid'
import Menu from './components/Menu'
import './style.scss'

const LEVEL = {
     easy: {
          bombs: 10,
          rows: 10,
          columns: 10,
     },
     medium: {
          bombs: 20,
          rows: 14,
          columns: 14,
     },
     hard: {
          bombs: 40,
          rows: 20,
          columns: 20,

     }
}

export default function Minesweeper({ values }) {

     const [isGameOver, setIsGameOver] = useState(false)
     const [difficulty, setDifficulty] = useState(LEVEL.easy)

     function changeDifficulty(diff) {
          setDifficulty(LEVEL.diff)
     }
     return (
          <Window values={values}>
               <div className="minesweeper">
                    <Menu changeDifficulty={changeDifficulty} />
                    {
                         !isGameOver &&
                         <Grid difficulty={difficulty} setIsGameOver={setIsGameOver} />
                    }
               </div>
          </Window>
     )
}
