import React from 'react'

export default function Menu({ changeDifficulty }) {



     return (
          <div>

               <li onClick={() => changeDifficulty('easy')}>Facile</li>
               <li onClick={() => changeDifficulty('medium')}>Intermédiaire</li>
               <li onClick={() => changeDifficulty('hard')}>Difficile</li>


          </div>
     )
}
