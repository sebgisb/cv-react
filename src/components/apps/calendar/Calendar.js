import React from 'react'
import Window from '../components/window/Window'
import './calendar.scss'
export default function Calendar({ values }) {
     return (
          <Window values={values}>
               <div className='calendar'> Calendar</div>
          </Window >
     )
}
