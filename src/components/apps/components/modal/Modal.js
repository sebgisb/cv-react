import React, { useState } from 'react'
import './modal.scss'

export default function Modal({ children, trigger, gameStart }) {

     const [isOpen, setIsOpen] = useState(false)

     const triggerModal = () => {
          setIsOpen(!isOpen)
     }

     return (
          <>
               <div onClick={triggerModal} className="trigger">{trigger}</div>
               {isOpen &&

                    <div className="modal">

                         {children}

                         <div onClick={gameStart} className="valid">Commencer</div>
                    </div>
               }
          </>
     )
}
