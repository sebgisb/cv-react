import React, { useEffect, useState, useContext, useRef } from 'react'

import './window.scss'

export default function Window({ values, children }) {

     const expand = 'expand.svg'
     const minus = 'minus.svg'
     const close = 'close.svg'
     const draggable = useRef()
     const dragged = useRef()

     useEffect(() => {
          setCurrentWindow()
     }, [])

     useEffect(() => {
          dragElement(draggable.current, dragged.current)

     }, [dragged])

     useEffect(() => {
          setCurrentWindow()
     }, [])

     useEffect(() => {
          if (values.isFullScreen) {
               dragged.current.style.width = 100 + "%"
               dragged.current.style.height = "100%"
               dragged.current.style.maxWidth = 'initial'
               dragged.current.style.maxHeight = 'initial'
               dragged.current.style.left = 0
               dragged.current.style.top = 0
          } else {
               dragged.current.style.width = 'initial'
               dragged.current.style.maxWidth = '100%'
               dragged.current.style.maxHeight = '100%'

               if (!dragged.current.contains(document.querySelector('.netflix, .curriculum '))) {
                    dragged.current.style.height = 'initial'
               } else {
                    dragged.current.style.height = 80 + '%'
               }
               dragged.current.style.left = Math.random() * 10 + 10 + '%'
               dragged.current.style.top = Math.random() * 10 + 10 + '%'
          }
     }, [values.isFullScreen])


     function setCurrentWindow() {

          if (dragged.current.classList.contains('current-window')) return


          let oldWindow = document.querySelector('.current-window')
          if (oldWindow) {

               oldWindow.classList.remove('current-window')
          }
          dragged.current.classList.add('current-window')

     }

     function toggleFullscreen() {
          values.setIsFullScreen(!values.isFullScreen)
     }


     const toggleFullcSreenDoubleClick = event => {
          if (event.detail === 2) {
               values.setIsFullScreen(!values.isFullScreen)
          }
     };

     useEffect(() => {
          if (dragged) {
               if (!values.isMinimized) {
                    if (dragged.current.classList.contains("minimize")) {
                         dragged.current.classList.add("enlarge")
                         setCurrentWindow()
                    }
                    dragged.current.classList.remove("minimize")
               } else {
                    dragged.current.classList.add("minimize")
                    dragged.current.classList.remove("enlarge")
               }
          }
     }, [values.isMinimized])

     return (

          <div className="window" ref={dragged} onClick={() => setCurrentWindow()}>

               <div className="window-toolbar" ref={draggable} onClick={toggleFullcSreenDoubleClick}>
                    <div className="title">
                         <img src={'icones/' + values.icone} alt={values.title + 'icone'} />
                         <h4 >{values.title}</h4>

                    </div>

                    <ul >
                         <li className="window-control" onClick={() => values.setIsMinimized(!values.isMinimized)}> <img src={'icones/' + minus} alt="Réduire" /></li>
                         <li className="window-control" onClick={() => toggleFullscreen()}>  <img src={'icones/' + expand} alt="agrandir" /></li>
                         <li className="window-control close" onClick={() => values.setIsOpen(false)} > <img src={'icones/' + close} alt="fermer" /></li>
                    </ul>

               </div>

               <div className="window-content">
                    {children}
               </div>
          </div>
     )
}



function dragElement(draggable, dragged) {

     let pos1 = 0,
          pos2 = 0,
          pos3 = 0,
          pos4 = 0;
     draggable.onpointerdown = pointerDrag;

     function pointerDrag(e) {
          e.preventDefault();
          pos3 = e.clientX;
          pos4 = e.clientY;
          document.onpointermove = elementDrag;
          document.onpointerup = stopElementDrag;
     }


     function elementDrag(e) {

          pos1 = pos3 - e.clientX;
          pos2 = pos4 - e.clientY;
          pos3 = e.clientX;
          pos4 = e.clientY;
          dragged.style.top =
               Math.round((dragged.offsetTop - pos2) * dragged.offsetHeight) /
               dragged.offsetHeight +
               "px";
          dragged.style.left =
               Math.round((dragged.offsetLeft - pos1) * dragged.offsetWidth) /
               dragged.offsetWidth +
               "px";
     }

     function stopElementDrag() {
          document.onpointerup = null;
          document.onpointermove = null;
     }
}


