import React from 'react'
import Window from '../components/window/Window'
import './style.scss'
export default function Curriculum({ values }) {
     return (
          <Window values={values} >
               <div className="curriculum">
                    <a href='file/cvGisbertSebastien.pdf' target="_blank" download className="cv-download">

                         <img src={"icones/download.png"} alt="télécharger cv" />
                    </a>

                    <div className="cv-content">

                         <div className="cv-header">
                              <h1>Gisbert Sébastien</h1>
                              <ul>
                                   <li>07 79 80 16 35</li>
                                   <li>sebastien992@gmail.com</li>
                              </ul>
                         </div>

                         <div className="cv-body">

                              <div className="group">


                                   <div>Expériences professionnels:</div>
                                   <section className="section section-pro">
                                        <ul>
                                             <li>
                                                  <h3>Bobygo | Millas</h3>
                                                  <p>décembre – février 2021</p>
                                                  <p>Stage développeur web</p>
                                             </li>
                                             <li>
                                                  <h3>Mas de Nages | Caissargues</h3>
                                                  <p>juin – octobre 2019</p>
                                                  <p>Manutentionnaire</p>
                                             </li>
                                             <li>
                                                  <h3>École élémentaire Lakanal | Nîmes</h3>
                                                  <p>novembre – juillet 2018</p>
                                                  <p>Service civique : Secrétariat / Assistant de vie scolaire</p>
                                             </li>
                                             <li>
                                                  <h3>Gîte d’étape d’Air de Côte | Lozère</h3>
                                                  <p>avril – août 2016</p>
                                                  <p>Factotum</p>
                                             </li>
                                             <li>
                                                  <h3>BGM France | Nîmes</h3>
                                                  <p>8 semaines de stage</p>
                                                  <p>Prestations techniques son & lumière</p>
                                             </li>
                                             <li>
                                                  <h3>Avenue PC | Nîmes</h3>
                                                  <p>12 semaines de stage</p>
                                                  <p>Maintenance et vente de matériel informatique</p>
                                             </li>
                                             <li>
                                                  <h3></h3>
                                                  <p></p>
                                                  <p></p>
                                             </li>

                                        </ul>
                                   </section>
                              </div>

                              <div className="group">



                                   <div>Formations :</div>
                                   <section className="section section-formation">
                                        <ul>
                                             <li>
                                                  <h3> Formations Développeur web / diplôme de niveau 5 - L’idem</h3>
                                                  <p>2020 – 2021</p>

                                             </li>
                                             <li>
                                                  <h3>Licence psychologie – Université de Nîmes </h3>
                                                  <p>2014 – 2016</p>

                                             </li>
                                             <li>
                                                  <h3>Première année licence AES – Université de Nîmes
                                                  </h3>
                                                  <p> 2013 – 2014</p>
                                             </li>

                                             <li>
                                                  <h3> Première année BTS Iris – CCI de Nîmes
                                                  </h3>
                                                  <p>2012 – 2013</p>

                                             </li>


                                        </ul>
                                   </section>
                              </div>
                         </div>


                    </div>
               </div>

          </Window>
     )
}
