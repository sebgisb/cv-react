import React from 'react'
import Window from '../components/window/Window'
import './git.scss'

export default function Git({ values }) {

     const openGit = () => {
          window.open('https://gitlab.com/gitlab-org/gitlab')
     }

     return (
          <Window values={values}>
               <div className='git'>
                    <p>Ouvrir Gitlab dans un nouvel onglet ?</p>
                    <div className="btn" onClick={openGit}>Ouvir</div>
               </div>
          </Window >
     )
}
