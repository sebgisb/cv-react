import React, { useEffect, useState, useRef } from 'react'
import Window from '../components/window/Window'
import Menu from './components/Menu'
import Grid from './components/Grid'
import './style.scss'


const LEVEL = {
     easy: {
          bombs: 10,
          rows: 10,
          columns: 10,
     },
     medium: {
          bombs: 18,
          rows: 12,
          columns: 12,
     },
     hard: {
          bombs: 25,
          rows: 12,
          columns: 15,

     }
}

export default function Minesweeper({ values }) {

     const [difficulty, setDifficulty] = useState(LEVEL.easy)

     useEffect(() => {
          console.log(difficulty)
     }, [difficulty])

     return (
          <Window values={values}>
               <div className="minesweeper">
                    <div>
                         <div className="minesweeper-menu">
                              <p>Difficulté</p>
                              <ul>

                                   <li onClick={() => setDifficulty(LEVEL.easy)}>Facile</li>
                                   <li onClick={() => setDifficulty(LEVEL.medium)}>Intermédiaire</li>
                                   <li onClick={() => setDifficulty(LEVEL.hard)}>Difficile</li>
                              </ul>
                         </div>
                         <Grid difficulty={difficulty} />
                    </div>
               </div>
          </Window>
     )
}
