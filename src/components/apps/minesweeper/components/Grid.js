import React, { useEffect, useRef, useState } from 'react'
import Stopwatch from './Stopwatch';

export default function Grid({ difficulty }) {

     const gridRef = useRef()
     let tilesArray = [];
     const [flagCount, setFlagCount] = useState()
     const [isGameOver, setIsGameOver] = useState(false)
     const [time, setTime] = useState({ s: 0, m: 0 })
     const [sucess, setSucess] = useState(false)
     const [hasGameStarded, setHasGameStarted] = useState(false)

     useEffect(() => {
          return () => {
               let tiles = document.querySelectorAll('.tile')
               tiles.forEach((tile) => {
                    tile.removeEventListener('click', click)
                    tile.removeEventListener('contextmenu', addFlag)
               })
          }
     }, [])

     useEffect(() => {
          resetGrid()
     }, [difficulty])

     function handleGameStart() {
          setHasGameStarted(true)
          gridRef.current.removeEventListener('click', handleGameStart)
     }

     function resetGrid() {
          setHasGameStarted(false)
          setSucess(false)
          setIsGameOver(true)
          setTime({ s: 0, m: 0 })
          let tiles = document.querySelectorAll('.tile')
          tiles.forEach((tile) => {
               tile.removeEventListener('click', click)
               tile.removeEventListener('contextmenu', addFlag)
          })
          setFlagCount(difficulty.bombs)
          gridRef.current.innerHTML = ""
          generateTiles()
     }

     function generateTiles() {
          gridRef.current.addEventListener('click', handleGameStart)
          setIsGameOver(false)
          let emptyArray = Array((difficulty.rows * difficulty.columns) - difficulty.bombs).fill('valid');
          let bombsArray = Array(difficulty.bombs).fill('bomb');
          let mixedArray = bombsArray.concat(emptyArray);
          let shuffledArray = mixedArray.sort(() => Math.random() - 0.5)
          let multiplier = 0;

          for (let r = 0; r < difficulty.rows; r++) {

               let row = document.createElement('div')
               row.setAttribute('class', 'row')
               gridRef.current.appendChild(row)

               for (let c = 0; c < difficulty.columns; c++) {

                    let tile = document.createElement('div')

                    tile.setAttribute('class', 'c' + c + ' r' + r + ' tile ' + shuffledArray[c + multiplier])
                    tile.setAttribute('data-c', c)
                    tile.setAttribute('data-r', r)

                    row.appendChild(tile)

                    tilesArray.push(tile)

                    tile.addEventListener('click', click)
                    tile.addEventListener('contextmenu', addFlag)
               }
               multiplier += difficulty.columns
          }
     }

     function click(event) {
          let tile;
          if (event.target) {
               tile = event.target
          } else {
               tile = event
          }

          if (tile && !tile.classList.contains('flag') && !tile.classList.contains('checked')) {
               setTimeout(() => {
                    checkTileContent(tile)

               }, 30)
          }
     }

     function checkTileContent(tile) {
          if (tile.classList.contains('bomb')) {
               gameOver()
               return
          }

          let total = 0;
          let r = parseInt(tile.getAttribute('data-r'))
          let c = parseInt(tile.getAttribute('data-c'))

          if (tile.classList.contains('valid')) {

               let left = document.querySelector('[data-c = "' + (c - 1) + '"][data-r = "' + r + '"]')
               if (left && left.classList.contains('bomb')) { total += 1 }

               let right = document.querySelector('[data-c = "' + (c + 1) + '"][data-r = "' + r + '"]')
               if (right && right.classList.contains('bomb')) { total += 1 }


               let topLeft = document.querySelector('[data-c = "' + (c - 1) + '"][data-r = "' + (r - 1) + '"]')
               if (topLeft && topLeft.classList.contains('bomb')) { total += 1 }

               let top = document.querySelector('[data-c = "' + c + '"][data-r = "' + (r - 1) + '"]')
               if (top && top.classList.contains('bomb')) { total += 1 }

               let topRight = document.querySelector('[data-c = "' + (c + 1) + '"][data-r = "' + (r - 1) + '"]')
               if (topRight && topRight.classList.contains('bomb')) { total += 1 }

               let bottomLeft = document.querySelector('[data-c = "' + (c - 1) + '"][data-r = "' + (r + 1) + '"]')
               if (bottomLeft && bottomLeft.classList.contains('bomb')) total += 1

               let bottom = document.querySelector('[data-c = "' + (c - 0) + '"][data-r = "' + (r + 1) + '"]')
               if (bottom && bottom.classList.contains('bomb')) { total += 1 }

               let bottomRight = document.querySelector('[data-c = "' + (c + 1) + '"][data-r = "' + (r + 1) + '"]')
               if (bottomRight && bottomRight.classList.contains('bomb')) { total += 1 }

               tile.classList.add('checked')

               tile.setAttribute('data-total', total)
               tile.classList.add('n' + total)

               if (total == 0) {
                    if (bottom && !bottom.getAttribute('data-total') > 0) click(bottom)
                    if (bottomRight && !bottomRight.getAttribute('data-total') > 0) click(bottomRight)
                    if (bottomLeft && !bottomLeft.getAttribute('data-total') > 0) click(bottomLeft)
                    if (top && !top.getAttribute('data-total') > 0) click(top)
                    if (topRight && !topRight.getAttribute('data-total') > 0) click(topRight)
                    if (topLeft && !topLeft.getAttribute('data-total') > 0) click(topLeft)
                    if (right && !right.getAttribute('data-total') > 0) click(right)
                    if (left && !left.getAttribute('data-total') > 0) click(left)
               } else {
                    tile.innerHTML = total

               }
          }
     }

     function addFlag(evt) {
          evt.preventDefault()
          let flags = document.querySelectorAll('.flag')
          let tile = evt.target

          if (flags.length === difficulty.bombs && !tile.classList.contains('flag')) {
               return
          }

          if (!tile.classList.contains('checked')) {
               if (!tile.classList.contains('flag')) {
                    tile.classList.add('flag')
                    tile.innerHTML = ' 🚩'
                    setFlagCount(flagCount => flagCount - 1)
               } else {
                    tile.classList.remove('flag')
                    tile.innerHTML = ''
                    setFlagCount(flagCount => flagCount + 1)
               }
          }

          let bombsNflags = document.querySelectorAll('.bomb.flag')
          console.log(bombsNflags.length)

          if (bombsNflags.length === difficulty.bombs) {
               setSucess(true)
               setHasGameStarted(false)
               setIsGameOver(true)
          }

     }

     function gameOver() {
          setHasGameStarted(false)
          setIsGameOver(true)
          let bombs = document.querySelectorAll('.bomb')
          let tiles = document.querySelectorAll('.tile')

          gridRef.current.removeEventListener('contextmenu', function (e) {
               e.preventDefault()
          })
          tiles.forEach((tile) => {
               tile.removeEventListener('click', click)
               tile.removeEventListener('contextmenu', addFlag)
          })

          for (let b = 0; b < bombs.length; b++) {
               bombs[b].classList.add('checked')
               bombs[b].innerHTML = "&#x1F4A3;"
          }
     }


     return (
          <div className="board">
               <div className="interface">

                    <div className="flags item">{flagCount}</div>
                    <div className="smiley item" onClick={resetGrid}>
                         {sucess || (!sucess && !isGameOver) ? <div><img src="icones/happy.png" alt="restart" /></div> : <div><img src="icones/unhappy.png" alt="restart" /></div>}
                    </div>

                    <div className="timer item"> <Stopwatch time={time} hasGameStarded={hasGameStarded} isGameOver={isGameOver} /></div>


               </div>
               <div className='grid'>

                    {!hasGameStarded && sucess ? <div className="minesweeper-success"><div>Félicitation</div></div> : ''}
                    <div ref={gridRef}>

                    </div>
               </div>
          </div>
     )
}
