import React, { useState, useEffect } from 'react'

export default function Stopwatch({ hasGameStarded, isGameOver }) {

     let updatedMin = 0
     let updatedSec = 0

     const [chrono, setChrono] = useState({ s: 0, m: 0 })



     useEffect(() => {
          const interval = setInterval(() => {
               run()
          }, 1000)

          if (hasGameStarded) {
               setChrono({ s: 0, m: 0 })
          } else {
               clearInterval(interval)
               if (!isGameOver) {
                    setChrono({ s: 0, m: 0 })
               }
          }

          return () => {
               clearInterval(interval)
               console.log('intervalCleared')
          }
     }, [hasGameStarded, isGameOver])

     const run = () => {
          updatedSec++
          if (updatedSec === 60) {
               updatedSec = 0
               updatedMin++
          }
          setChrono({ s: updatedSec, m: updatedMin })
     }

     return (
          <div className="stopwatch">
               <>
                    <span>{chrono.m >= 10 ? chrono.m : '0' + chrono.m}</span>
                    :
                    <span>{chrono.s >= 10 ? chrono.s : '0' + chrono.s}</span>
               </>

          </div>
     )
}
