import React from 'react'
import useFetch from '../../../../../hook/useFetch'
import MaListe from '../movielist/MaListe'
import MovieList from '../movielist/MovieList'
import './body.scss'
import Slider from '../Slider/Slider'
export default function Body() {

     const [genres, pending, error] = useFetch('https://api.themoviedb.org/3/genre/movie/list?api_key=7e85e6aadc52bb4bbd63228098331e01&language=en-US')

     return (
          <div className="netflix-body">
               {genres &&
                    <>
                         <MaListe />
                         {genres.genres.map(genre => {
                              return <Slider key={genre.id} categorie={genre.name} />
                              // return <MovieList key={genre.id} categorie={genre.name} />
                         })}
                    </>
               }
          </div>
     )
}
