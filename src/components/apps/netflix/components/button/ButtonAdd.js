import React, { useContext } from 'react'
import './style.scss';
import { NetflixContext } from '../../Netflix'

export default function ButtonAdd({ movie }) {

     const { addToList } = useContext(NetflixContext)

     return (
          <img onClick={() => addToList(movie)} className={'btn-add little-btn'} src="icones/btn-add.png" alt="info" />
     )
}
