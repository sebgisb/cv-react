import React, { useState, useEffect, useContext } from 'react'
import { NetflixContext } from '../../Netflix'
import ButtonAdd from '../button/ButtonAdd'
import ButtonRemove from '../button/ButtonRemove'
import './style.scss';

export default function ButtonHandleList({ movie }) {


     const [isInList, setIsInList] = useState(false)
     const { myList } = useContext(NetflixContext)

     useEffect(() => {
          for (let i = 0; i < myList.length; i++) {
               if (myList[i].id == movie.id) {
                    setIsInList(true)
                    break
               }
               setIsInList(false)
          }
     }, [myList])

     return (
          <>
               {!isInList ?
                    <ButtonAdd movie={movie} />
                    :
                    <ButtonRemove movie={movie} />
               }</>
     )
}
