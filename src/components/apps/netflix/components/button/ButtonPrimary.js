import React, { useContext } from 'react'
import './style.scss';
import { NetflixContext } from '../../Netflix'

export default function ButtonPrimary({ movie }) {

     const { setCurrentWatchedMovie } = useContext(NetflixContext)
     return (
          <div onClick={() => setCurrentWatchedMovie(movie)} className={'netflix-btn primary'}><img src="icones/play.png" alt="info" />Lecture</div>
     )
}
