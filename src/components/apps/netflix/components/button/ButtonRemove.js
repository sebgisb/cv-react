import React, { useContext } from 'react'
import './style.scss';
import { NetflixContext } from '../../Netflix'

export default function ButtonRemove({ movie }) {

     const { removeFromList } = useContext(NetflixContext)
     return (
          <img onClick={() => removeFromList(movie)} className={'btn-remove little-btn'} src="icones/btn-remove.png" alt="info" />
     )
}
