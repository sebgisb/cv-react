import React, { useContext } from 'react'
import './style.scss';
import { NetflixContext } from '../../Netflix'

export default function ButtonSecondary({ movie }) {

     const { setCurrentMovieDetails } = useContext(NetflixContext)

     return (
          <div onClick={() => setCurrentMovieDetails(movie)} className={'netflix-btn secondary'}><img src="icones/info.png" alt="info" />Plus d'info</div>
     )
}
