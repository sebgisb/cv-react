import React, { useContext } from 'react'
import './style.scss';
import { NetflixContext } from '../../Netflix'

export default function LittleButtonPrimary({ movie }) {

     const { setCurrentWatchedMovie } = useContext(NetflixContext)
     return (
          <img onClick={() => setCurrentWatchedMovie(movie)} className={'little-btn-primary little-btn'} src="icones/play-btn.png" alt="info" />
     )
}
