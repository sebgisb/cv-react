import React, { useContext } from 'react'
import { NetflixContext } from '../../Netflix'
import './style.scss';
export default function LittleButtonSecondary({ movie }) {

     const { setCurrentMovieDetails } = useContext(NetflixContext)

     return (
          <img onClick={() => setCurrentMovieDetails(movie)} className='little-btn-secondary little-btn' src="icones/info.png" />
     )
}
