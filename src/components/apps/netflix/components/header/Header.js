import React from 'react'
import { useEffect, useRef, useState } from 'react'
import MovieDetails from '../moviedetails/MovieDetails'
import WatchMovie from '../watchmovie/WatchMovie'
import './header.scss'
import ButtonSecondary from '../button/ButtonSecondary'
import ButtonPrimary from '../button/ButtonPrimary'

export default function Header({ data }) {

     const [background, setBackground] = useState(data.backdrop_path)
     const [quality, setQuality] = useState('original')

     const selfRef = useRef()
     let el;
     let desktop = document.querySelector('.desktop')

     useEffect(() => {

          selfRef.current.style.height = (desktop.getBoundingClientRect().height) - 50 + 'px';


          el = selfRef.current

          const resize_ob = new ResizeObserver(function (el) {
               if (selfRef.current.getBoundingClientRect().width < 500) {
                    setBackground(data.poster_path)
                    setQuality('w500')
               } else {
                    setBackground(data.backdrop_path)
                    setQuality('original')
               }
               selfRef.current.style.height = desktop.getBoundingClientRect().height - 40 + 'px';
          })

          resize_ob.observe(selfRef.current)
          return () => {
               resize_ob.unobserve(el)

          }
     }, [])

     function resizeHeader() {
          selfRef.current.style.height = (desktop.getBoundingClientRect().height) - 70 + 'px';
     }

     return (
          <div ref={selfRef} className="netflix-header" style={{ backgroundImage: 'url(https://image.tmdb.org/t/p/' + quality + '/' + background + ')' }}>

               <div className="netflix-header-content">

                    <h1>{data.title}</h1>
                    <p>{data.overview}</p>
                    <div className="netflix-btn-container">
                         <ButtonPrimary movie={data} />
                         <ButtonSecondary movie={data} />
                    </div>
               </div>
          </div>
     )
}
