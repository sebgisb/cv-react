import React from 'react'
import './menu.scss'

export default function Menu() {
     return (
          <ul className="netflix-menu">
               <div>
                    <img className="netflix-logo" src="icones/netflix-logo.png" />
                    <li>Accueil</li>
                    <li>Séries</li>
                    <li>Films</li>
                    <li>Ma liste</li>
               </div>
               <div>
                    <li>Search</li>
                    <li>User</li>
               </div>
          </ul>
     )
}
