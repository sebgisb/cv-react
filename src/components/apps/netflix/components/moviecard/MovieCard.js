import React, { useState, useContext, useEffect } from 'react'
import LittleButtonSecondary from '../button/LittleButtonSecondary'
import LittleButtonPrimary from '../button/LittleButtonPrimary'
import './style.scss'
import ButtonHandleList from '../button/ButtonHandleList'

export default function MovieCard({ movie, categories }) {

     return (

          <div className='movie-card'>
               <div className="movie-card-content" style={{ backgroundImage: 'url(https://image.tmdb.org/t/p/w500' + movie.poster_path + ')' }}>

                    <div className="movie-card-hover-content">
                         <div className='movie-card-btn'>
                              <div>
                                   <LittleButtonPrimary movie={movie} />

                                   <ButtonHandleList movie={movie} />


                              </div>
                              <LittleButtonSecondary movie={movie} />
                         </div>
                         <p className="movie-note">{movie.vote_average} / 10</p>
                         <div className="movie-categories">

                              {categories.map((cat, key) => {
                                   return <div className="movie-category" key={key}>{cat} </div>
                              })}
                         </div>
                    </div>
               </div>
          </div>

     )
}
