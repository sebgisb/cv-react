import './style.scss'
import React, { useState, useEffect, useRef, useContext } from 'react'
import ButtonSecondary from '../button/ButtonSecondary'
import ButtonPrimary from '../button/ButtonPrimary'
import SimilarMovies from './SimilarMovies'
import MovieTrailer from './MovieTrailer'


export default function MovieDetails({ movie, setIsOpen, isOpen }) {

     const modalRef = useRef()
     const selfRef = useRef()

     let currentWidth = document.querySelector('.netflix').getBoundingClientRect().width
     let currentHeight = document.querySelector('.netflix').getBoundingClientRect().height
     let windowRect = document.querySelector('.current-window').getBoundingClientRect()

     useEffect(() => {

          document.querySelector('.app ').addEventListener('click', closePanel)

          modalRef.current.style.left = windowRect.left + 'px'
          modalRef.current.style.right = windowRect.right + 'px'
          modalRef.current.style.top = windowRect.top + 5 + 'px'
          modalRef.current.style.width = windowRect.width - 20 + 'px'
          modalRef.current.style.height = windowRect.height - 5 + 'px'


          const resize_ob = new ResizeObserver(function () {

               if (isOpen) {

                    if (currentWidth !== document.querySelector('.netflix ').getBoundingClientRect().width ||
                         currentHeight !== document.querySelector('.netflix ').getBoundingClientRect().height
                    ) {
                         setIsOpen(false)
                    }
               }
          })

          resize_ob.observe(document.querySelector('.netflix '))

          return () => {

               resize_ob.unobserve(document.querySelector('.netflix '))

               if (document.querySelector('.app ')) {
                    document.querySelector('.app ').removeEventListener('click', closePanel)
               }
          }
     }, [])


     function closePanel(evt) {
          if (!selfRef.current.contains(evt.target)) {
               setIsOpen(false)
          }
     }

     return (
          <div ref={modalRef} className='movie-details-modal netflix-modal'>

               <div ref={selfRef} className="movie-details">
                    <div className='movie-details-header'>
                         <MovieTrailer movie={movie} />
                    </div>

                    <div className='movie-details-body'>
                         <div className='movie-info'>
                              <p className="movie-note">Note: {Math.round(movie.vote_average, 1)}/10</p>
                              <p>Release date: {movie.release_date}</p>
                              <p>{movie.overview}</p>
                         </div>
                         <SimilarMovies movie={movie.id} film={movie} />
                    </div>
               </div>

          </div>
     )
}

