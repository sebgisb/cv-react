import React, { useContext } from 'react'
import ButtonPrimary from '../button/ButtonPrimary'

export default function MovieTrailer({ movie }) {

     return (
          <div className='movie-trailer' style={{ backgroundImage: 'url(https://image.tmdb.org/t/p/w500/' + movie.backdrop_path + ')' }}>
               <ButtonPrimary movie={movie} />
          </div>
     )
}
