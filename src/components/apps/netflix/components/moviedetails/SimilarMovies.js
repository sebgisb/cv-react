import React, { useContext } from 'react'
import LittleButtonPrimary from '../button/LittleButtonPrimary'
import './similarmovie.scss'
import { NetflixContext } from '../../Netflix'
import ButtonHandleList from '../button/ButtonHandleList'
const similarMovies = {
     "page": 1,
     "results": [
          {
               "adult": false,
               "backdrop_path": "/l4QHerTSbMI7qgvasqxP36pqjN6.jpg",
               "genre_ids": [
                    28,
                    878
               ],
               "id": 603,
               "original_language": "en",
               "original_title": "The Matrix",
               "overview": "Set in the 22nd century, The Matrix tells the story of a computer hacker who joins a group of underground insurgents fighting the vast and powerful computers who now rule the earth.",
               "popularity": 71.132,
               "poster_path": "/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg",
               "release_date": "1999-03-30",
               "title": "The Matrix",
               "video": false,
               "vote_average": 8.19,
               "vote_count": 22075
          },
          {
               "adult": false,
               "backdrop_path": "/pxK1iK6anS6erGg4QePmMKbB1E7.jpg",
               "genre_ids": [
                    12,
                    28,
                    53,
                    878
               ],
               "id": 604,
               "original_language": "en",
               "original_title": "The Matrix Reloaded",
               "overview": "Six months after the events depicted in The Matrix, Neo has proved to be a good omen for the free humans, as more and more humans are being freed from the matrix and brought to Zion, the one and only stronghold of the Resistance.  Neo himself has discovered his superpowers including super speed, ability to see the codes of the things inside the matrix and a certain degree of pre-cognition. But a nasty piece of news hits the human resistance: 250,000 machine sentinels are digging to Zion and would reach them in 72 hours. As Zion prepares for the ultimate war, Neo, Morpheus and Trinity are advised by the Oracle to find the Keymaker who would help them reach the Source.  Meanwhile Neo's recurrent dreams depicting Trinity's death have got him worried and as if it was not enough, Agent Smith has somehow escaped deletion, has become more powerful than before and has fixed Neo as his next target.",
               "popularity": 44.464,
               "poster_path": "/9TGHDvWrqKBzwDxDodHYXEmOE6J.jpg",
               "release_date": "2003-05-15",
               "title": "The Matrix Reloaded",
               "video": false,
               "vote_average": 7.025,
               "vote_count": 9288
          },
          {
               "adult": false,
               "backdrop_path": "/533xAMhhVyjTy8hwMUFEt5TuDfR.jpg",
               "genre_ids": [
                    12,
                    28,
                    53,
                    878
               ],
               "id": 605,
               "original_language": "en",
               "original_title": "The Matrix Revolutions",
               "overview": "The human city of Zion defends itself against the massive invasion of the machines as Neo fights to end the war at another front while also opposing the rogue Agent Smith.",
               "popularity": 38.107,
               "poster_path": "/fgm8OZ7o4G1G1I9EeGcb85Noe6L.jpg",
               "release_date": "2003-11-05",
               "title": "The Matrix Revolutions",
               "video": false,
               "vote_average": 6.701,
               "vote_count": 8343
          },
          {
               "adult": false,
               "backdrop_path": "/xgAEfHByamMdRCA31oERQrsiutF.jpg",
               "genre_ids": [
                    18,
                    36,
                    10752
               ],
               "id": 613,
               "original_language": "de",
               "original_title": "Der Untergang",
               "overview": "In April of 1945, Germany stands at the brink of defeat with the Russian Army closing in from the east and the Allied Expeditionary Force attacking from the west. In Berlin, capital of the Third Reich, Adolf Hitler proclaims that Germany will still achieve victory and orders his generals and advisers to fight to the last man. When the end finally does come, and Hitler lies dead by his own hand, what is left of his military must find a way to end the killing that is the Battle of Berlin, and lay down their arms in surrender.",
               "popularity": 17.069,
               "poster_path": "/cP1ElGjBhbZAAqmueXjHDKlSwiP.jpg",
               "release_date": "2004-09-16",
               "title": "Downfall",
               "video": false,
               "vote_average": 7.83,
               "vote_count": 3102
          },
          {
               "adult": false,
               "backdrop_path": "/4840rkbpsiuow5ew155oVKcqJwj.jpg",
               "genre_ids": [
                    18
               ],
               "id": 615,
               "original_language": "en",
               "original_title": "The Passion of the Christ",
               "overview": "A graphic portrayal of the last twelve hours of Jesus of Nazareth's life.",
               "popularity": 99.164,
               "poster_path": "/2C9vyK6leWDb2ds65R7uIwSmh8V.jpg",
               "release_date": "2004-02-25",
               "title": "The Passion of the Christ",
               "video": false,
               "vote_average": 7.422,
               "vote_count": 3557
          },
          {
               "adult": false,
               "backdrop_path": "/y8HaV1Ntr6nHmKxZ29B7DwxwvNU.jpg",
               "genre_ids": [
                    35,
                    80
               ],
               "id": 623,
               "original_language": "en",
               "original_title": "A Fish Called Wanda",
               "overview": "A diamond advocate is attempting to steal a collection of diamonds, yet troubles arise when he realizes that he is not the only one after the diamonds.",
               "popularity": 15.685,
               "poster_path": "/hkSGFNVfEEUXFCxRZDITFHVhUlu.jpg",
               "release_date": "1988-07-15",
               "title": "A Fish Called Wanda",
               "video": false,
               "vote_average": 7.215,
               "vote_count": 1733
          },
          {
               "adult": false,
               "backdrop_path": "/e9GbzPawXDFvLwSseCXpHkbCyAP.jpg",
               "genre_ids": [
                    18,
                    80
               ],
               "id": 627,
               "original_language": "en",
               "original_title": "Trainspotting",
               "overview": "Heroin addict Mark Renton stumbles through bad ideas and sobriety attempts with his unreliable friends -- Sick Boy, Begbie, Spud and Tommy. He also has an underage girlfriend, Diane, along for the ride. After cleaning up and moving from Edinburgh to London, Mark finds he can't escape the life he left behind when Begbie shows up at his front door on the lam, and a scheming Sick Boy follows.",
               "popularity": 25.116,
               "poster_path": "/bhY62Dw8iW54DIhxPQerbuB9DOP.jpg",
               "release_date": "1996-02-23",
               "title": "Trainspotting",
               "video": false,
               "vote_average": 7.974,
               "vote_count": 8370
          },
          {
               "adult": false,
               "backdrop_path": "/3fChciF2G1wXHsyTfJD9y7uN6Il.jpg",
               "genre_ids": [
                    27,
                    18,
                    14
               ],
               "id": 628,
               "original_language": "en",
               "original_title": "Interview with the Vampire",
               "overview": "A vampire relates his epic life story of love, betrayal, loneliness, and dark hunger to an over-curious reporter.",
               "popularity": 40.407,
               "poster_path": "/2162lAT2MP36MyJd2sttmj5du5T.jpg",
               "release_date": "1994-11-11",
               "title": "Interview with the Vampire",
               "video": false,
               "vote_average": 7.38,
               "vote_count": 4827
          },
          {
               "adult": false,
               "backdrop_path": "/qCU097AXMPAsmpVK4j9kzMTxt2d.jpg",
               "genre_ids": [
                    12,
                    14,
                    10751
               ],
               "id": 630,
               "original_language": "en",
               "original_title": "The Wizard of Oz",
               "overview": "Young Dorothy finds herself in a magical world where she makes friends with a lion, a scarecrow and a tin man as they make their way along the yellow brick road to talk with the Wizard and ask for the things they miss most in their lives. The Wicked Witch of the West is the only thing that could stop them.",
               "popularity": 59.77,
               "poster_path": "/bSA6DbAC5gdkaooU164lQUX6rVs.jpg",
               "release_date": "1939-08-15",
               "title": "The Wizard of Oz",
               "video": false,
               "vote_average": 7.58,
               "vote_count": 4638
          },
          {
               "adult": false,
               "backdrop_path": "/77GJuRgBByi9TNgGXEex2ZCrHxR.jpg",
               "genre_ids": [
                    18,
                    53,
                    9648
               ],
               "id": 638,
               "original_language": "en",
               "original_title": "Lost Highway",
               "overview": "A tormented jazz musician finds himself lost in an enigmatic story involving murder, surveillance, gangsters, doppelgängers, and an impossible transformation inside a prison cell.",
               "popularity": 16.196,
               "poster_path": "/5POhfNeFPIi4VUNwCTaK85sh98r.jpg",
               "release_date": "1997-01-15",
               "title": "Lost Highway",
               "video": false,
               "vote_average": 7.598,
               "vote_count": 2025
          },
          {
               "adult": false,
               "backdrop_path": "/n7p6UTAZtkeoHkwCO42BEQaMFJY.jpg",
               "genre_ids": [
                    18,
                    878,
                    12
               ],
               "id": 644,
               "original_language": "en",
               "original_title": "A.I. Artificial Intelligence",
               "overview": "David, a robotic boy—the first of his kind programmed to love—is adopted as a test case by a Cybertronics employee and his wife. Though he gradually becomes their child, a series of unexpected circumstances make this life impossible for David. Without final acceptance by humans or machines, David embarks on a journey to discover where he truly belongs, uncovering a world in which the line between robot and machine is both vast and profoundly thin.",
               "popularity": 25.182,
               "poster_path": "/8MZSGX5JORoO72EfuAEcejH5yHn.jpg",
               "release_date": "2001-06-29",
               "title": "A.I. Artificial Intelligence",
               "video": false,
               "vote_average": 7.027,
               "vote_count": 5308
          },
          {
               "adult": false,
               "backdrop_path": "/ptcXbbxTyjHfEE4d4QocNP0YQct.jpg",
               "genre_ids": [
                    12,
                    28,
                    53
               ],
               "id": 646,
               "original_language": "en",
               "original_title": "Dr. No",
               "overview": "In the film that launched the James Bond saga, Agent 007 battles mysterious Dr. No, a scientific genius bent on destroying the U.S. space program. As the countdown to disaster begins, Bond must go to Jamaica, where he encounters beautiful Honey Ryder, to confront a megalomaniacal villain in his massive island headquarters.",
               "popularity": 28.087,
               "poster_path": "/f9HsemSsBEHN5eoMble1bj6fDxs.jpg",
               "release_date": "1962-10-07",
               "title": "Dr. No",
               "video": false,
               "vote_average": 7,
               "vote_count": 2911
          },
          {
               "adult": false,
               "backdrop_path": "/8uNjWHsSW3TSOWDU4EY76H5MzAG.jpg",
               "genre_ids": [
                    28,
                    53,
                    12
               ],
               "id": 657,
               "original_language": "en",
               "original_title": "From Russia with Love",
               "overview": "Agent 007 is back in the second installment of the James Bond series, this time battling a secret crime organization known as SPECTRE. Russians Rosa Klebb and Kronsteen are out to snatch a decoding device known as the Lektor, using the ravishing Tatiana to lure Bond into helping them. Bond willingly travels to meet Tatiana in Istanbul, where he must rely on his wits to escape with his life in a series of deadly encounters with the enemy.",
               "popularity": 25.681,
               "poster_path": "/zx4V17FP8oclNvOpTgs2iCCtiYk.jpg",
               "release_date": "1963-10-10",
               "title": "From Russia with Love",
               "video": false,
               "vote_average": 7.075,
               "vote_count": 2390
          },
          {
               "adult": false,
               "backdrop_path": "/sU3gFo7Gn67zjtKXIvzJ4omcUbH.jpg",
               "genre_ids": [
                    28,
                    18,
                    36
               ],
               "id": 665,
               "original_language": "en",
               "original_title": "Ben-Hur",
               "overview": "In 25 AD,Judah Ben-Hur, a Jew in ancient Judea, opposes the occupying Roman empire.  Falsely accused by a Roman childhood friend-turned-overlord of trying to kill the Roman governor, he is put into slavery and his mother and sister are taken away as prisoners.  Three years later and freed by a grateful Roman galley commander whom he has rescued from drowning, he becomes an expert charioteer for Rome, all the while plotting to return to Judea, find and rescue his family, and avenge himself on his former friend.  All the while, the form and work of Jesus move in the background of his life...",
               "popularity": 36.722,
               "poster_path": "/m4WQ1dBIrEIHZNCoAjdpxwSKWyH.jpg",
               "release_date": "1959-11-18",
               "title": "Ben-Hur",
               "video": false,
               "vote_average": 7.849,
               "vote_count": 2166
          },
          {
               "adult": false,
               "backdrop_path": "/tcbzsdWpdidbN7zbOymlRKfIHVg.jpg",
               "genre_ids": [
                    18,
                    53,
                    9648,
                    28
               ],
               "id": 670,
               "original_language": "ko",
               "original_title": "올드보이",
               "overview": "With no clue how he came to be imprisoned, drugged and tortured for 15 years, a desperate businessman seeks revenge on his captors.",
               "popularity": 38.618,
               "poster_path": "/pWDtjs568ZfOTMbURQBYuT4Qxka.jpg",
               "release_date": "2003-11-21",
               "title": "Oldboy",
               "video": false,
               "vote_average": 8.271,
               "vote_count": 6924
          },
          {
               "adult": false,
               "backdrop_path": "/t3LicFpYHeYpwqm7L5wDpd22hL5.jpg",
               "genre_ids": [
                    12,
                    14
               ],
               "id": 671,
               "original_language": "en",
               "original_title": "Harry Potter and the Philosopher's Stone",
               "overview": "Harry Potter has lived under the stairs at his aunt and uncle's house his whole life. But on his 11th birthday, he learns he's a powerful wizard—with a place waiting for him at the Hogwarts School of Witchcraft and Wizardry. As he learns to harness his newfound powers with the help of the school's kindly headmaster, Harry uncovers the truth about his parents' deaths—and about the villain who's to blame.",
               "popularity": 254.173,
               "poster_path": "/wuMc08IPKEatf9rnMNXvIDxqP4W.jpg",
               "release_date": "2001-11-16",
               "title": "Harry Potter and the Philosopher's Stone",
               "video": false,
               "vote_average": 7.916,
               "vote_count": 23233
          },
          {
               "adult": false,
               "backdrop_path": "/5rrGVmRUuCKVbqUu41XIWTXJmNA.jpg",
               "genre_ids": [
                    12,
                    14,
                    10751
               ],
               "id": 674,
               "original_language": "en",
               "original_title": "Harry Potter and the Goblet of Fire",
               "overview": "When Harry Potter's name emerges from the Goblet of Fire, he becomes a competitor in a grueling battle for glory among three wizarding schools—the Triwizard Tournament. But since Harry never submitted his name for the Tournament, who did? Now Harry must confront a deadly dragon, fierce water demons and an enchanted maze only to find himself in the cruel grasp of He Who Must Not Be Named.",
               "popularity": 244.369,
               "poster_path": "/fECBtHlr0RB3foNHDiCBXeg9Bv9.jpg",
               "release_date": "2005-11-16",
               "title": "Harry Potter and the Goblet of Fire",
               "video": false,
               "vote_average": 7.822,
               "vote_count": 17697
          },
          {
               "adult": false,
               "backdrop_path": "/pyTDtQCn5sG8gi2zkM5ZylSNmtJ.jpg",
               "genre_ids": [
                    12,
                    28,
                    53
               ],
               "id": 681,
               "original_language": "en",
               "original_title": "Diamonds Are Forever",
               "overview": "Diamonds are stolen only to be sold again in the international market. James Bond infiltrates a smuggling mission to find out who's guilty. The mission takes him to Las Vegas where Bond meets his archenemy Blofeld.",
               "popularity": 22.078,
               "poster_path": "/uajiHcFX5sOhYB2tBuWkmizbtuU.jpg",
               "release_date": "1971-12-13",
               "title": "Diamonds Are Forever",
               "video": false,
               "vote_average": 6.4,
               "vote_count": 1655
          },
          {
               "adult": false,
               "backdrop_path": "/vcj47nTm1fDlkxUwDb2GgYbKDDW.jpg",
               "genre_ids": [
                    12,
                    28,
                    53
               ],
               "id": 682,
               "original_language": "en",
               "original_title": "The Man with the Golden Gun",
               "overview": "Cool government operative James Bond searches for a stolen invention that can turn the sun's heat into a destructive weapon. He soon crosses paths with the menacing Francisco Scaramanga, a hitman so skilled he has a seven-figure working fee. Bond then joins forces with the swimsuit-clad Mary Goodnight, and together they track Scaramanga to a tropical isle hideout where the killer-for-hire lures the slick spy into a deadly maze for a final duel.",
               "popularity": 25.443,
               "poster_path": "/zULDLrGE42iTiizMJKDoTGbIKlu.jpg",
               "release_date": "1974-12-01",
               "title": "The Man with the Golden Gun",
               "video": false,
               "vote_average": 6.43,
               "vote_count": 1605
          },
          {
               "adult": false,
               "backdrop_path": "/x5Z2BjuUfEEwYWZccDwbU9KSEqj.jpg",
               "genre_ids": [
                    18,
                    878,
                    9648
               ],
               "id": 686,
               "original_language": "en",
               "original_title": "Contact",
               "overview": "A radio astronomer receives the first extraterrestrial radio signal ever picked up on Earth. As the world powers scramble to decipher the message and decide upon a course of action, she must make some difficult decisions between her beliefs, the truth, and reality.",
               "popularity": 31.558,
               "poster_path": "/bCpMIywuNZeWt3i5UMLEIc0VSwM.jpg",
               "release_date": "1997-07-11",
               "title": "Contact",
               "video": false,
               "vote_average": 7.406,
               "vote_count": 3652
          }
     ],
     "total_pages": 500,
     "total_results": 10000
}
export default function SimilarMovies({ film }) {
     const { setCurrentWatchedMovie } = useContext(NetflixContext)
     // const [similarMovies, similarPending, similarError] = useFetch(`https://api.themoviedb.org/3/movie/550/similar?api_key=7e85e6aadc52bb4bbd63228098331e01`)


     return (
          <div>
               <h2>Titres similaire</h2>
               <div className="similar-movies">
                    {similarMovies && similarMovies.results.map((movie, key) => {

                         if (key < 15) {

                              return <div className="similar-movie-card"
                                   key={movie.id}>
                                   <div className="similar-movie-img" style={{ backgroundImage: 'url(https://image.tmdb.org/t/p/w500/' + movie.backdrop_path + ')' }}>


                                        {/* <LittleButtonPrimary movie={movie} /> */}
                                        <div onClick={() => setCurrentWatchedMovie(movie)}><img className="play-btn" src="icones/play-btn.png" /></div>
                                        {/* <div></div> */}

                                   </div>
                                   <div className="similiar-movie-body">
                                        <ButtonHandleList movie={movie} />
                                        <p className="movie-note">Note: {Math.round(movie.vote_average, 1)}/10</p>
                                        <p>{movie.release_date}</p>

                                        <p>{movie.overview.substring(0, 180)}</p>

                                   </div>
                              </div>
                         }
                    })}
               </div>
          </div>
     )
}
