import React, { useEffect, useState, useContext } from 'react'
import MovieCard from '../moviecard/MovieCard'
import { NetflixContext } from '../../Netflix'
const myMovieList = [
     {
          "adult": false,
          "backdrop_path": "/2RSirqZG949GuRwN38MYCIGG4Od.jpg",
          "genre_ids": [
               53
          ],
          "id": 985939,
          "original_language": "en",
          "original_title": "Fall",
          "overview": "For best friends Becky and Hunter, life is all about conquering fears and pushing limits. But after they climb 2,000 feet to the top of a remote, abandoned radio tower, they find themselves stranded with no way down. Now Becky and Hunter’s expert climbing skills will be put to the ultimate test as they desperately fight to survive the elements, a lack of supplies, and vertigo-inducing heights",
          "popularity": 8419.389,
          "poster_path": "/9f5sIJEgvUpFv0ozfA6TurG4j22.jpg",
          "release_date": "2022-08-11",
          "title": "Fall",
          "video": false,
          "vote_average": 7.4,
          "vote_count": 625
     },
     {
          "adult": false,
          "backdrop_path": "/skL7c4ZhZqo1IFbMcHNrol4fvkc.jpg",
          "genre_ids": [
               28,
               80,
               53
          ],
          "id": 921360,
          "original_language": "en",
          "original_title": "Wire Room",
          "overview": "New recruit Justin Rosa must monitor arms-smuggling cartel member Eddie Flynn — and keep him alive at all costs. When a SWAT team descends on Flynn’s home, Rosa breaks protocol and contacts the gangster directly to save his life. As gunmen break into the Wire Room and chaos erupts, Mueller and Rosa make a final, desperate stand against the corrupt agents and officials who seek to destroy evidence and kill them both.",
          "popularity": 2914.108,
          "poster_path": "/b9ykj4v8ykjRoGB7SpI1OuxblNU.jpg",
          "release_date": "2022-09-02",
          "title": "Wire Room",
          "video": false,
          "vote_average": 7.2,
          "vote_count": 54
     },
]


export default function MaListe() {

     const [isLoading, setIsLoading] = useState(true)
     const { genres, myList } = useContext(NetflixContext)


     useEffect(() => {

          if (genres != null && myList != null) {
               setIsLoading(false)
          }
     }, [genres, myList])





     return (

          <div className="movie-list">
               <div className="movie-list-head">
                    <h3>Ma liste</h3>
               </div>
               <div className="movie-list-body">
                    {!isLoading && myList.length > 0 ? <>

                         {myList.map(movie => {
                              let length = genres.genres.length
                              let categories = []

                              for (let i = 0; i < movie.genre_ids.length; i++) {
                                   if (categories.length > 2) {

                                        break
                                   }
                                   for (let c = 0; c < length; c++) {

                                        if (movie.genre_ids[i] === genres.genres[c].id) {
                                             categories.push(genres.genres[c].name)
                                        }
                                   }
                              }
                              return <MovieCard key={movie.id} movie={movie} categories={categories} />
                         })}


                    </> : <h4 style={{ textAlign: 'center' }}>Your list is empty</h4>
                    }
               </div>
          </div>


     )
}
