import React, { useState, useEffect, useContext, useRef } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore from 'swiper'

import useFetch from '../../../../../hook/useFetch'
import MovieCard from '../moviecard/MovieCard'
import './movielist.scss'
import { NetflixContext } from '../../Netflix'

const movies = {
     "page": 1,
     "results": [
          {
               "adult": false,
               "backdrop_path": "/2RSirqZG949GuRwN38MYCIGG4Od.jpg",
               "genre_ids": [
                    53
               ],
               "id": 985939,
               "original_language": "en",
               "original_title": "Fall",
               "overview": "For best friends Becky and Hunter, life is all about conquering fears and pushing limits. But after they climb 2,000 feet to the top of a remote, abandoned radio tower, they find themselves stranded with no way down. Now Becky and Hunter’s expert climbing skills will be put to the ultimate test as they desperately fight to survive the elements, a lack of supplies, and vertigo-inducing heights",
               "popularity": 8419.389,
               "poster_path": "/9f5sIJEgvUpFv0ozfA6TurG4j22.jpg",
               "release_date": "2022-08-11",
               "title": "Fall",
               "video": false,
               "vote_average": 7.4,
               "vote_count": 625
          },
          {
               "adult": false,
               "backdrop_path": "/skL7c4ZhZqo1IFbMcHNrol4fvkc.jpg",
               "genre_ids": [
                    28,
                    80,
                    53
               ],
               "id": 921360,
               "original_language": "en",
               "original_title": "Wire Room",
               "overview": "New recruit Justin Rosa must monitor arms-smuggling cartel member Eddie Flynn — and keep him alive at all costs. When a SWAT team descends on Flynn’s home, Rosa breaks protocol and contacts the gangster directly to save his life. As gunmen break into the Wire Room and chaos erupts, Mueller and Rosa make a final, desperate stand against the corrupt agents and officials who seek to destroy evidence and kill them both.",
               "popularity": 2914.108,
               "poster_path": "/b9ykj4v8ykjRoGB7SpI1OuxblNU.jpg",
               "release_date": "2022-09-02",
               "title": "Wire Room",
               "video": false,
               "vote_average": 7.2,
               "vote_count": 54
          },
          {
               "adult": false,
               "backdrop_path": "/7ZO9yoEU2fAHKhmJWfAc2QIPWJg.jpg",
               "genre_ids": [
                    53,
                    28,
                    878
               ],
               "id": 766507,
               "original_language": "en",
               "original_title": "Prey",
               "overview": "When danger threatens her camp, the fierce and highly skilled Comanche warrior Naru sets out to protect her people. But the prey she stalks turns out to be a highly evolved alien predator with a technically advanced arsenal.",
               "popularity": 2546.261,
               "poster_path": "/ujr5pztc1oitbe7ViMUOilFaJ7s.jpg",
               "release_date": "2022-08-02",
               "title": "Prey",
               "video": false,
               "vote_average": 8,
               "vote_count": 3840
          },
          {
               "adult": false,
               "backdrop_path": "/AfvIjhDu9p64jKcmohS4hsPG95Q.jpg",
               "genre_ids": [
                    27,
                    53
               ],
               "id": 756999,
               "original_language": "en",
               "original_title": "The Black Phone",
               "overview": "Finney Blake, a shy but clever 13-year-old boy, is abducted by a sadistic killer and trapped in a soundproof basement where screaming is of little use. When a disconnected phone on the wall begins to ring, Finney discovers that he can hear the voices of the killer’s previous victims. And they are dead set on making sure that what happened to them doesn’t happen to Finney.",
               "popularity": 1580.995,
               "poster_path": "/lr11mCT85T1JanlgjMuhs9nMht4.jpg",
               "release_date": "2022-06-22",
               "title": "The Black Phone",
               "video": false,
               "vote_average": 7.9,
               "vote_count": 2473
          },
          {
               "adult": false,
               "backdrop_path": "/5GA3vV1aWWHTSDO5eno8V5zDo8r.jpg",
               "genre_ids": [
                    27,
                    53
               ],
               "id": 760161,
               "original_language": "en",
               "original_title": "Orphan: First Kill",
               "overview": "After escaping from an Estonian psychiatric facility, Leena Klammer travels to America by impersonating Esther, the missing daughter of a wealthy family. But when her mask starts to slip, she is put against a mother who will protect her family from the murderous “child” at any cost.",
               "popularity": 1522.763,
               "poster_path": "/wSqAXL1EHVJ3MOnJzMhUngc8gFs.jpg",
               "release_date": "2022-07-27",
               "title": "Orphan: First Kill",
               "video": false,
               "vote_average": 7.1,
               "vote_count": 541
          },
          {
               "adult": false,
               "backdrop_path": "/pkLegAR3G5T63B2Xz2ijbD1BAm8.jpg",
               "genre_ids": [
                    27,
                    53,
                    28,
                    12,
                    80,
                    9648,
                    18,
                    10752
               ],
               "id": 927341,
               "original_language": "en",
               "original_title": "Hunting Ava Bravo",
               "overview": "Billionaire sportsman Buddy King unwinds by hunting human captives on his remote mountain estate. But his latest victim, Ava Bravo is no easy target.",
               "popularity": 1456.054,
               "poster_path": "/etc6HKBEhNySNnYU2nRgbSeIyoW.jpg",
               "release_date": "2022-04-01",
               "title": "Hunting Ava Bravo",
               "video": false,
               "vote_average": 6.5,
               "vote_count": 56
          },
          {
               "adult": false,
               "backdrop_path": "/xVbppM1xgbskOKgOuV8fbWBWHtt.jpg",
               "genre_ids": [
                    27,
                    9648,
                    878,
                    53
               ],
               "id": 762504,
               "original_language": "en",
               "original_title": "Nope",
               "overview": "Residents in a lonely gulch of inland California bear witness to an uncanny, chilling discovery.",
               "popularity": 1380.346,
               "poster_path": "/AcKVlWaNVVVFQwro3nLXqPljcYA.jpg",
               "release_date": "2022-07-20",
               "title": "Nope",
               "video": false,
               "vote_average": 7,
               "vote_count": 1433
          },
          {
               "adult": false,
               "backdrop_path": "/eiBMu2noc6r1QZLfQlFhfVtbXeM.jpg",
               "genre_ids": [
                    28,
                    53,
                    9648
               ],
               "id": 997120,
               "original_language": "en",
               "original_title": "Sniper: Rogue Mission",
               "overview": "When a crooked federal agent is involved in a human sex trafficking ring, Sniper and CIA Rookie Brandon Beckett goes rogue, teaming up with his former allies Homeland Security Agent Zero and assassin Lady Death to uncover the corrupt agent and take down the criminal organization.",
               "popularity": 1327.854,
               "poster_path": "/jSL5TqroJsDAIgBdELBwby1uhf1.jpg",
               "release_date": "2022-08-15",
               "title": "Sniper: Rogue Mission",
               "video": false,
               "vote_average": 6.9,
               "vote_count": 57
          },
          {
               "adult": false,
               "backdrop_path": "/6FbWhRR5B7OWVgzXuTkIFiaI2N3.jpg",
               "genre_ids": [
                    28,
                    53
               ],
               "id": 848123,
               "original_language": "en",
               "original_title": "Black Site",
               "overview": "A group of officers based in a labyrinthine top-secret prison must fight for their lives against Hatchet, a brilliant and infamous high-value detainee. When he escapes, his mysterious and deadly agenda has far reaching and dire consequences.",
               "popularity": 1282.442,
               "poster_path": "/ipn8khVVC4eToWiGf89WF9J5PJn.jpg",
               "release_date": "2022-05-05",
               "title": "Black Site",
               "video": false,
               "vote_average": 7.2,
               "vote_count": 97
          },
          {
               "adult": false,
               "backdrop_path": "/mCAkUizRRIwKMOlne9CIoJ3utSc.jpg",
               "genre_ids": [
                    53,
                    10770,
                    28
               ],
               "id": 951368,
               "original_language": "en",
               "original_title": "Your Boyfriend Is Mine",
               "overview": "Over the objection of his girlfriend, Ben agrees to take a job as the “live in” man servant to a wealthy businesswoman, Amanda, but quickly realizes he has made a deal with the devil, and has put himself and his girlfriend in mortal jeopardy",
               "popularity": 1256.302,
               "poster_path": "/2OOYNZLKjdX8Z5KNyz7zZnHmodJ.jpg",
               "release_date": "2022-03-19",
               "title": "Your Boyfriend Is Mine",
               "video": false,
               "vote_average": 6.4,
               "vote_count": 23
          },
          {
               "adult": false,
               "backdrop_path": "/zlLQXKsSa2CEkz3UBFWPUPX90PN.jpg",
               "genre_ids": [
                    53
               ],
               "id": 968857,
               "original_language": "en",
               "original_title": "Looks Can Kill",
               "overview": "A group of models is killed off, one by one, and everyone is a suspect.",
               "popularity": 1235.53,
               "poster_path": "/1uy2PNFwtkqH3mhGd6irk5aeIrF.jpg",
               "release_date": "2022-02-01",
               "title": "Looks Can Kill",
               "video": false,
               "vote_average": 6.1,
               "vote_count": 6
          },
          {
               "adult": false,
               "backdrop_path": "/qJQSB0UDYW3XdKSTcclI1kdp3hZ.jpg",
               "genre_ids": [
                    53
               ],
               "id": 952374,
               "original_language": "en",
               "original_title": "The Aviary",
               "overview": "A twisted journey of two women’s desperate flee to escape the clutches of Skylight, an insidious cult. Lured in by the promise of “freedom” in the isolated desert campus called The Aviary, Jillian and Blair join forces to escape in hopes of real freedom. Consumed by fear and paranoia, they can’t shake the feeling that they are being followed by the cult’s leader, Seth, a man as seductive as he is controlling. The more distance the pair gains from the cult, the more Seth holds control of their minds. With supplies dwindling and their senses failing, Jillian and Blair are faced with a horrifying question: how do you run from an enemy who lives inside your head?",
               "popularity": 1160.412,
               "poster_path": "/u6HUQcOQsgkFFO8xCITfxQz6ivc.jpg",
               "release_date": "2022-04-29",
               "title": "The Aviary",
               "video": false,
               "vote_average": 5.8,
               "vote_count": 14
          },
          {
               "adult": false,
               "backdrop_path": "/jazlkwXfw4KdF6fVTRsolOvRCmu.jpg",
               "genre_ids": [
                    53,
                    12,
                    9648,
                    28,
                    80
               ],
               "id": 924482,
               "original_language": "en",
               "original_title": "The Ledge",
               "overview": "A rock climbing adventure between two friends turns into a terrifying nightmare. After Kelly captures the murder of her best friend on camera, she becomes the next target of a tight knit group of friends who will stop at nothing to destroy the evidence and anyone in their way. Desperate for her safety, she begins a treacherous climb up a mountain cliff and her survival instincts are put to the test when she becomes trapped with the killers just 20 feet away.",
               "popularity": 1024.026,
               "poster_path": "/dHKfsdNcEPw7YIWFPIhqiuWrSAb.jpg",
               "release_date": "2022-02-18",
               "title": "The Ledge",
               "video": false,
               "vote_average": 6.2,
               "vote_count": 99
          },
          {
               "adult": false,
               "backdrop_path": "/r26Qj2HfAv6rPXzULLpdDWrkwrT.jpg",
               "genre_ids": [
                    53
               ],
               "id": 852448,
               "original_language": "en",
               "original_title": "I Came By",
               "overview": "A rebellious young graffiti artist, who targets the homes of the wealthy elite, discovers a shocking secret that leads him on a journey endangering himself and those closest to him.",
               "popularity": 914.788,
               "poster_path": "/856bLLUvEYu3dRDXCCoRE7oxO0V.jpg",
               "release_date": "2022-08-19",
               "title": "I Came By",
               "video": false,
               "vote_average": 6.2,
               "vote_count": 203
          },
          {
               "adult": false,
               "backdrop_path": "/1U5zz2Jvt2L7hXHf1ZN1n74Zx8j.jpg",
               "genre_ids": [
                    28,
                    53,
                    80
               ],
               "id": 769636,
               "original_language": "es",
               "original_title": "Código Emperador",
               "overview": "Follows Juan, an agent working for the intelligence services, who also reports to a parallel unit involved in illegal activities.",
               "popularity": 912.782,
               "poster_path": "/8VjVLMiPm598Kg6XmKk5m1fz0p7.jpg",
               "release_date": "2022-03-18",
               "title": "Code Name: Emperor",
               "video": false,
               "vote_average": 5.9,
               "vote_count": 96
          },
          {
               "adult": false,
               "backdrop_path": "/b0PlSFdDwbyK0cf5RxwDpaOJQvQ.jpg",
               "genre_ids": [
                    80,
                    9648,
                    53
               ],
               "id": 414906,
               "original_language": "en",
               "original_title": "The Batman",
               "overview": "In his second year of fighting crime, Batman uncovers corruption in Gotham City that connects to his own family while facing a serial killer known as the Riddler.",
               "popularity": 785.498,
               "poster_path": "/74xTEgt7R36Fpooo50r9T25onhq.jpg",
               "release_date": "2022-03-01",
               "title": "The Batman",
               "video": false,
               "vote_average": 7.7,
               "vote_count": 6193
          },
          {
               "adult": false,
               "backdrop_path": "/21ldFAokKVjwzi1SHXoPb5gc7md.jpg",
               "genre_ids": [
                    27,
                    9648,
                    53
               ],
               "id": 760104,
               "original_language": "en",
               "original_title": "X",
               "overview": "In 1979, a group of young filmmakers set out to make an adult film in rural Texas, but when their reclusive, elderly hosts catch them in the act, the cast find themselves fighting for their lives.",
               "popularity": 695.429,
               "poster_path": "/A7YPhQKdcr6XB1kCUdS4tHifYWd.jpg",
               "release_date": "2022-03-17",
               "title": "X",
               "video": false,
               "vote_average": 6.7,
               "vote_count": 931
          },
          {
               "adult": false,
               "backdrop_path": "/tTxBcogfOEvU9MfOFjdHx5pWXnz.jpg",
               "genre_ids": [
                    53,
                    10770
               ],
               "id": 950618,
               "original_language": "en",
               "original_title": "Sinister Stepsister",
               "overview": "Jeff Mitchell seems to have it all. He has an interesting and lucrative job, a loving wife, and two amazing kids. Therefore, life is good. However, on one bright day, a cosmic sucker punch comes out of the blue. When Jeff was in high school, unbeknownst to him, he got his girlfriend pregnant. Next, the girl moved away, had the baby, and raised her as a single mother, never telling her daughter, Carlee, who her real father was. However, Carlee's mother recently died in a tragic accident, prompting Carlee to try and uncover the truth. Maybe Jeff is not so lucky after all?",
               "popularity": 686.773,
               "poster_path": "/2psm4URqYAF9jSnA1ux8BKmm0EI.jpg",
               "release_date": "2022-03-11",
               "title": "Sinister Stepsister",
               "video": false,
               "vote_average": 6.4,
               "vote_count": 16
          },
          {
               "adult": false,
               "backdrop_path": "/7Y3LdmsZukXhmwxtO0UF95BFBTt.jpg",
               "genre_ids": [
                    18,
                    9648,
                    53
               ],
               "id": 682507,
               "original_language": "en",
               "original_title": "Where the Crawdads Sing",
               "overview": "Abandoned by her family, Kya raises herself all alone in the marshes outside of her small town. When her former boyfriend is found dead, Kya is instantly branded by the local townspeople and law enforcement as the prime suspect for his murder.",
               "popularity": 657.276,
               "poster_path": "/n1el846gLDXfhOvrRCsyvaAOQWv.jpg",
               "release_date": "2022-07-15",
               "title": "Where the Crawdads Sing",
               "video": false,
               "vote_average": 7.5,
               "vote_count": 242
          },
          {
               "adult": false,
               "backdrop_path": "/5luJmy21N3bYCkjzRHsoZfvLtPP.jpg",
               "genre_ids": [
                    53,
                    18,
                    80
               ],
               "id": 773975,
               "original_language": "en",
               "original_title": "End of the Road",
               "overview": "Recently widowed mom Brenda fights to protect her family during a harrowing road trip when a murder and a missing bag of cash plunge them into danger.",
               "popularity": 653.407,
               "poster_path": "/tLFIMuPWJHlTJ6TN8HCOiSD6SdA.jpg",
               "release_date": "2022-09-09",
               "title": "End of the Road",
               "video": false,
               "vote_average": 6.7,
               "vote_count": 112
          }
     ],
     "total_pages": 1769,
     "total_results": 35363
}

export default function MovieList({ categorie }) {
     const [isLoading, setIsLoading] = useState(true)
     const genres = useContext(NetflixContext)
     // const [movies, pending, error] = useFetch(`https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&include_image_language=en,null&api_key=7e85e6aadc52bb4bbd63228098331e01&with_genres=${genre.id}`)
     const clamp = (num, min, max) => Math.min(Math.max(num, min), max);
     const sliderRef = useRef()
     useEffect(() => {
          if (genres.genres != null) {
               setIsLoading(false)
               // generateSlider()
          }
     }, [genres])



     return (
          <div className="movie-list">

               {!isLoading && <>

                    <div className="movie-list-head">
                         <h3>{categorie}</h3>
                    </div>
                    <div ref={sliderRef} className="movie-list-body">



                         {movies && movies.results.map(movie => {
                              let length = genres.genres.genres.length
                              let categories = []
                              for (let i = 0; i < movie.genre_ids.length; i++) {
                                   if (categories.length > 2) {
                                        break
                                   }
                                   for (let c = 0; c < length; c++) {

                                        if (movie.genre_ids[i] === genres.genres.genres[c].id) {
                                             categories.push(genres.genres.genres[c].name)
                                        }
                                   }

                              }
                              return (
                                   <MovieCard movie={movie} categories={categories} />
                              )
                         })}

                    </div>

               </>}

          </div>
     )
}
