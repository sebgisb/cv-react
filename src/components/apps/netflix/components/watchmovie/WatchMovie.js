import React, { useState, useEffect, useRef } from 'react'
import './watchmovie.scss'
import ButtonPrimary from '../button/ButtonPrimary'
import useFetch from '../../../../../hook/useFetch'

export default function WatchMovie({ movie, setIsOpen, isOpen }) {
     const [trailer, pending, error] = useFetch(`https://api.themoviedb.org/3/movie/${movie.id}/videos?api_key=7e85e6aadc52bb4bbd63228098331e01&language=en-US`)


     const [video, setVideo] = useState()

     useEffect(() => {
          if (trailer) {
               setVideo(trailer.results.filter(result => result.type === 'Trailer'))
          }
     }, [trailer])



     return (
          <div className="watch-movie">

               <img onClick={() => setIsOpen(false)} alt="close video" className="close-modal hide-arrow" src="icones/arrow.png" />

               {video &&
                    <iframe
                         className="lecteur"
                         id="ytplayer"
                         title='movie'
                         type="text/html"
                         width="640" height="360"
                         src={'https://www.youtube.com/embed/' + video[0].key + '?autoplay=1'}
                         frameBorder="0"
                    />
               }



          </div>


     )


}
