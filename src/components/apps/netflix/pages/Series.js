import React, { useEffect } from 'react'
import useFetch from '../../../../hook/useFetch'

const URL = `https://api.themoviedb.org/3/tv/popular?api_key=`
const API_KEY = '7e85e6aadc52bb4bbd63228098331e01'

const series = {
     "page": 1,
     "results": [
          {
               "backdrop_path": "/Aa9TLpNpBMyRkD8sPJ7ACKLjt0l.jpg",
               "first_air_date": "2022-08-21",
               "genre_ids": [
                    10765,
                    18,
                    10759
               ],
               "id": 94997,
               "name": "House of the Dragon",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "House of the Dragon",
               "overview": "The Targaryen dynasty is at the absolute apex of its power, with more than 15 dragons under their yoke. Most empires crumble from such heights. In the case of the Targaryens, their slow fall begins when King Viserys breaks with a century of tradition by naming his daughter Rhaenyra heir to the Iron Throne. But when Viserys later fathers a son, the court is shocked when Rhaenyra retains her status as his heir, and seeds of division sow friction across the realm.",
               "popularity": 8735.731,
               "poster_path": "/z2yahl2uefxDCl0nogcRBstwruJ.jpg",
               "vote_average": 8.6,
               "vote_count": 1255
          },
          {
               "backdrop_path": "/pdfCr8W0wBCpdjbZXSxnKhZtosP.jpg",
               "first_air_date": "2022-09-01",
               "genre_ids": [
                    10765,
                    10759,
                    18
               ],
               "id": 84773,
               "name": "The Lord of the Rings: The Rings of Power",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "The Lord of the Rings: The Rings of Power",
               "overview": "Beginning in a time of relative peace, we follow an ensemble cast of characters as they confront the re-emergence of evil to Middle-earth. From the darkest depths of the Misty Mountains, to the majestic forests of Lindon, to the breathtaking island kingdom of Númenor, to the furthest reaches of the map, these kingdoms and characters will carve out legacies that live on long after they are gone.",
               "popularity": 5407.801,
               "poster_path": "/suyNxglk17Cpk8rCM2kZgqKdftk.jpg",
               "vote_average": 7.6,
               "vote_count": 679
          },
          {
               "backdrop_path": "/9GvhICFMiRQA82vS6ydkXxeEkrd.jpg",
               "first_air_date": "2022-08-18",
               "genre_ids": [
                    35,
                    10765
               ],
               "id": 92783,
               "name": "She-Hulk: Attorney at Law",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "She-Hulk: Attorney at Law",
               "overview": "Jennifer Walters navigates the complicated life of a single, 30-something attorney who also happens to be a green 6-foot-7-inch superpowered hulk.",
               "popularity": 3515.825,
               "poster_path": "/hJfI6AGrmr4uSHRccfJuSsapvOb.jpg",
               "vote_average": 7.3,
               "vote_count": 720
          },
          {
               "backdrop_path": "/zymbuoBoL1i94xAOzVJF6IuWLfD.jpg",
               "first_air_date": "2018-05-02",
               "genre_ids": [
                    10759,
                    18,
                    35
               ],
               "id": 77169,
               "name": "Cobra Kai",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "Cobra Kai",
               "overview": "This Karate Kid sequel series picks up 30 years after the events of the 1984 All Valley Karate Tournament and finds Johnny Lawrence on the hunt for redemption by reopening the infamous Cobra Kai karate dojo. This reignites his old rivalry with the successful Daniel LaRusso, who has been working to maintain the balance in his life without mentor Mr. Miyagi.",
               "popularity": 2666.897,
               "poster_path": "/6POBWybSBDBKjSs1VAQcnQC1qyt.jpg",
               "vote_average": 8.2,
               "vote_count": 5002
          },
          {
               "backdrop_path": "/uGy4DCmM33I7l86W7iCskNkvmLD.jpg",
               "first_air_date": "2013-12-02",
               "genre_ids": [
                    16,
                    35,
                    10765,
                    10759
               ],
               "id": 60625,
               "name": "Rick and Morty",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "Rick and Morty",
               "overview": "Rick is a mentally-unbalanced but scientifically gifted old man who has recently reconnected with his family. He spends most of his time involving his young grandson Morty in dangerous, outlandish adventures throughout space and alternate universes. Compounded with Morty's already unstable family life, these events cause Morty much distress at home and school.",
               "popularity": 2075.747,
               "poster_path": "/cvhNj9eoRBe5SxjCbQTkh05UP5K.jpg",
               "vote_average": 8.7,
               "vote_count": 6990
          },
          {
               "backdrop_path": "/rIe3PnM6S7IBUmvNwDkBMX0i9EZ.jpg",
               "first_air_date": "2011-04-17",
               "genre_ids": [
                    10765,
                    18,
                    10759
               ],
               "id": 1399,
               "name": "Game of Thrones",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "Game of Thrones",
               "overview": "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.",
               "popularity": 1325.389,
               "poster_path": "/u3bZgnGQ9T01sWNhyveQz0wH0Hl.jpg",
               "vote_average": 8.4,
               "vote_count": 19141
          },
          {
               "backdrop_path": "/8aJjd7X4UfpxqtV7ukiAqBgTaQz.jpg",
               "first_air_date": "2022-08-24",
               "genre_ids": [],
               "id": 208883,
               "name": "The Investigator",
               "origin_country": [
                    "CN"
               ],
               "original_language": "zh",
               "original_name": "简言的夏冬",
               "overview": "A story about risk management follows a team of investigators who work together to eliminate fraud.\n\nXia Dong who works as an investigator for a global company that specializes in risk management has always considered professional ethics to be his life's mission. In order to complete a job that was assigned to him and to uncover the truth behind an incident that got him framed while studying abroad, Xia Dong transfers from US headquarters to the Shanghai branch. In his new office, he encounters Jian Yan, his junior at school who now works as an investigator.  In the following days and nights, Xia Dong's righteousness and kindness shines through and easily helps in resolving the misunderstandings from the past. Soon after, Xia Dong and Jian Yan are conducting a background check on a company and find themselves facing repeated threats on their life after they helped Lin Jun Wen, an old classmate.",
               "popularity": 1302.631,
               "poster_path": "/74Z6Ka23i1lKd4ytqXY1nzqVBz.jpg",
               "vote_average": 8.5,
               "vote_count": 2
          },
          {
               "backdrop_path": "/o8zk3QmHYMSC7UiJgFk81OFF1sc.jpg",
               "first_air_date": "2022-08-22",
               "genre_ids": [
                    10766,
                    18
               ],
               "id": 204095,
               "name": "Mar do Sertão",
               "origin_country": [
                    "BR"
               ],
               "original_language": "pt",
               "original_name": "Mar do Sertão",
               "overview": "",
               "popularity": 1273.304,
               "poster_path": "/ixgnqO8xhFMb1zr8RRFsyeZ9CdD.jpg",
               "vote_average": 5.6,
               "vote_count": 7
          },
          {
               "backdrop_path": "/jW61BMd1ZVWPXI7Ts4iC2BTx8Qj.jpg",
               "first_air_date": "2022-04-11",
               "genre_ids": [
                    18
               ],
               "id": 196572,
               "name": "The Secret House",
               "origin_country": [
                    "KR"
               ],
               "original_language": "ko",
               "original_name": "The Secret House",
               "overview": "A story about quitting being a good boy and chasing evil to the end for the sake of the mother and sister who sacrificed for themselves.\n\nA revenge play in which a dirt spoon lawyer chasing the traces of his missing mother walks into the secret surrounding him to fight the world.",
               "popularity": 1067.563,
               "poster_path": "/wNN9hJDV45BF2PiJQQVwBCiqLIl.jpg",
               "vote_average": 5.6,
               "vote_count": 13
          },
          {
               "backdrop_path": "/kOkmTrD8kWLeTXcAEwctg6GwW7t.jpg",
               "first_air_date": "2017-02-20",
               "genre_ids": [
                    10751,
                    35
               ],
               "id": 91759,
               "name": "Come Home Love: Lo and Behold",
               "origin_country": [
                    "HK"
               ],
               "original_language": "cn",
               "original_name": "Come Home Love: Lo and Behold",
               "overview": "Hung Sue Gan starting from the bottom, established his own logistics company, which is now running smoothly. His only concern now are his three daughters. His eldest daughter has immigrated overseas. His second daughter Hung Yeuk Shui has reached the marriageable age, but has no hopes for marriage anytime soon. She is constantly bickering with her younger sister Hung Sum Yue, who is an honour student, over trivial matters, causing their father to not know whether to laugh or cry. Hung Sue Yan, Hung Sue Gan's brother, moves in with the family, temporarily ending his life as a nomadic photographer. He joins Hung Yeuk Shui's company and encounters Ko Pak Fei, the director of an online shop. The two appear to be former lovers, making for lots of laughter. Since Hung Sue Yan moved in, a series of strange events have occurred in the family. Upon investigation, the source is traced to Lung Ging Fung, a promising young man who is the son of department store mogul Lung Gam Wai.",
               "popularity": 1013.148,
               "poster_path": "/lgD4j9gUGmMckZpWWRJjorWqGVT.jpg",
               "vote_average": 4.3,
               "vote_count": 10
          },
          {
               "backdrop_path": "/hLi5umdYp8VyoQqPE9QXCUsW0Ps.jpg",
               "first_air_date": "2000-07-03",
               "genre_ids": [
                    10764
               ],
               "id": 911,
               "name": "Kaun Banega Crorepati",
               "origin_country": [
                    "IN"
               ],
               "original_language": "hi",
               "original_name": "कौन बनेगा करोड़पति",
               "overview": "Hosted by India's biggest superstar, Amitabh Bachchan, one of the biggest shows is here to entertain millions, change lives and make dreams come true.",
               "popularity": 995.322,
               "poster_path": "/8srHjzgZq9V9sgsTkIwGARDyADj.jpg",
               "vote_average": 7.3,
               "vote_count": 20
          },
          {
               "backdrop_path": "/6Z2lcLK2E4pfsjgmYK3KHlqgHSI.jpg",
               "first_air_date": "2022-09-07",
               "genre_ids": [
                    18
               ],
               "id": 208925,
               "name": "Diary of a Gigolo",
               "origin_country": [
                    "AR",
                    "MX"
               ],
               "original_language": "es",
               "original_name": "Diario de un Gigoló",
               "overview": "A male escort's life begins to unravel when he gets involved in a client's family affairs and breaks the cardinal rule of his work: Don't fall in love.",
               "popularity": 984.115,
               "poster_path": "/xuqszO6iQXqBsD4kP2xhlBN5ggc.jpg",
               "vote_average": 8.5,
               "vote_count": 34
          },
          {
               "backdrop_path": "/eqhKMZTLcieAvoH6CBqknTTfNby.jpg",
               "first_air_date": "2022-08-05",
               "genre_ids": [
                    10765,
                    18
               ],
               "id": 90802,
               "name": "The Sandman",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "The Sandman",
               "overview": "After years of imprisonment, Morpheus — the King of Dreams — embarks on a journey across worlds to find what was stolen from him and restore his power.",
               "popularity": 942.849,
               "poster_path": "/q54qEgagGOYCq5D1903eBVMNkbo.jpg",
               "vote_average": 8.2,
               "vote_count": 900
          },
          {
               "backdrop_path": "/FbUIi5WeDU5j1EtipQ274tts1E.jpg",
               "first_air_date": "2022-04-11",
               "genre_ids": [
                    35,
                    18,
                    10751
               ],
               "id": 197352,
               "name": "Bravo, My Life",
               "origin_country": [
                    "KR"
               ],
               "original_language": "ko",
               "original_name": "으라차차 내 인생",
               "overview": "Even though Seo Dong Hee comes from a poor family background, she has a bright and positive personality. She dreams of becoming a designer and slowly works her way to achieve her dream. She decides to become the mother of her nephew and raise him by herself. She also gets involved with Kang Cha Yeol. Kang Cha Yeol’s father runs a big company. His father wants him to work for his company, but Kang Cha Yeol doesn't want to. He wants to go back to the U.S., but he realizes he can't do anything without his father's financial support. His father tells Kang Cha Yeol to work for 1 year at his company.",
               "popularity": 935.215,
               "poster_path": "/3ehMAk8E9zETA9r2ZydwVrgUoP3.jpg",
               "vote_average": 7,
               "vote_count": 3
          },
          {
               "backdrop_path": "/56v2KjBlU4XaOv9rVYEQypROD7P.jpg",
               "first_air_date": "2016-07-15",
               "genre_ids": [
                    18,
                    10765,
                    9648
               ],
               "id": 66732,
               "name": "Stranger Things",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "Stranger Things",
               "overview": "When a young boy vanishes, a small town uncovers a mystery involving secret experiments, terrifying supernatural forces, and one strange little girl.",
               "popularity": 897.508,
               "poster_path": "/49WJfeN0moxb9IPfGn8AIqMGskD.jpg",
               "vote_average": 8.6,
               "vote_count": 13640
          },
          {
               "backdrop_path": "/x0RRnWdYeczF4KXDqW8blda7SKS.jpg",
               "first_air_date": "2022-03-28",
               "genre_ids": [
                    18,
                    10766
               ],
               "id": 158415,
               "name": "Pantanal",
               "origin_country": [
                    "BR"
               ],
               "original_language": "pt",
               "original_name": "Pantanal",
               "overview": "After the mysterious disappearance of his father, Joventino, the cowboy José Leôncio becomes a wealthy farm owner in Pantanal. Over twenty years have passed and, bitter-hearted because of his father vanishing and the escape of his wife to Rio de Janeiro with his baby, José Leôncio has the chance to make amends with the boy, now a young man raised in the big city with very different values and habits than his own.",
               "popularity": 886.452,
               "poster_path": "/4Ru1BwcAKWtpM345dWe8YgHf35V.jpg",
               "vote_average": 5.7,
               "vote_count": 49
          },
          {
               "backdrop_path": "/xZUZ9i6vVayjyhR1vRo9Bjku4h.jpg",
               "first_air_date": "2016-01-25",
               "genre_ids": [
                    80,
                    10765
               ],
               "id": 63174,
               "name": "Lucifer",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "Lucifer",
               "overview": "Bored and unhappy as the Lord of Hell, Lucifer Morningstar abandoned his throne and retired to Los Angeles, where he has teamed up with LAPD detective Chloe Decker to take down criminals. But the longer he's away from the underworld, the greater the threat that the worst of humanity could escape.",
               "popularity": 870.132,
               "poster_path": "/ekZobS8isE6mA53RAiGDG93hBxL.jpg",
               "vote_average": 8.5,
               "vote_count": 12458
          },
          {
               "backdrop_path": "/7J5d7K4JBDZ4IDODNX3THLggtM5.jpg",
               "first_air_date": "2005-03-27",
               "genre_ids": [
                    18
               ],
               "id": 1416,
               "name": "Grey's Anatomy",
               "origin_country": [
                    "US"
               ],
               "original_language": "en",
               "original_name": "Grey's Anatomy",
               "overview": "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.",
               "popularity": 847.397,
               "poster_path": "/zPIug5giU8oug6Xes5K1sTfQJxY.jpg",
               "vote_average": 8.3,
               "vote_count": 8403
          },
          {
               "backdrop_path": "/escBySi78E2rcrphN973x7txmJq.jpg",
               "first_air_date": "2022-08-15",
               "genre_ids": [
                    10759,
                    10765,
                    18
               ],
               "id": 121750,
               "name": "Darna",
               "origin_country": [
                    "PH"
               ],
               "original_language": "tl",
               "original_name": "Darna",
               "overview": "When fragments of a green crystal scatter in the city and turn people into destructive monsters, Narda embraces her destiny as Darna—the mighty protector of the powerful stone from Planet Marte.",
               "popularity": 825.809,
               "poster_path": "/CFOce6pbb3FRNaBaVdvNsCv5kR.jpg",
               "vote_average": 3,
               "vote_count": 5
          },
          {
               "backdrop_path": "/hNwopoqRDazw1uPHY2jSNmV2gS2.jpg",
               "first_air_date": "2005-06-06",
               "genre_ids": [
                    10763,
                    10764,
                    99
               ],
               "id": 203599,
               "name": "Scoop",
               "origin_country": [
                    "HK"
               ],
               "original_language": "cn",
               "original_name": "東張西望",
               "overview": "Scoop is a comprehensive information programme of Television Broadcasts Limited.\n\nThe content of the program is mainly based on entertainment news and personal follow-up of the artists, and will also be interspersed with the latest trends of TVB dramas and artists. Some entertainment news content clips will be rebroadcast on the next day's \"Entertainment Live\".\n\nThis program will be broadcast on Jade Channel from 19:30-20:00 (Hong Kong time) from June 6, 2005, and will be broadcast every day from March 3, 2019, and will be broadcast on myTV (later myTV SUPER) to provide \"Program Review\".",
               "popularity": 823.689,
               "poster_path": "/qo6y0XvSBlKM3XCbYVdcR3a6qyQ.jpg",
               "vote_average": 8.8,
               "vote_count": 6
          }
     ],
     "total_pages": 6849,
     "total_results": 136964
}

export default function Series({ children }) {

     // const [series, pending, error] = useFetch(URL + API_KEY)

     useEffect(() => {
          console.log(series)
     }, [series])

     return (
          <div className="series">{children}</div>
     )
}
