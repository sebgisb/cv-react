import React, { useState, useEffect } from 'react'

export default function Stopwatch({ time }) {


     // useEffect(() => { console.log('hey') }, [time])

     return (
          <div className="stopwatch">


               <span>{time.m >= 10 ? time.m : '0' + time.m}</span>
               :
               <span>{time.s >= 10 ? time.s : '0' + time.s}</span>


          </div>
     )
}
