import React from 'react'
import Window from '../components/window/Window'
import './params.scss'

export default function Params({ values }) {

     function handleBackgroundChange(background) {
          values.setBackground(background)
     }
     function handleSizeMultiplierChange(multiplier) {
          values.setSizeMultiplier(multiplier)
     }

     return (
          <Window values={values} >
               <div className="params">

                    <div className="form-group">
                         <label htmlFor="background">Fond d'écran</label>
                         <select name="background" value={values.background} onChange={(e) => handleBackgroundChange(e.target.value)}>
                              <option value="desert.jpg">Désert</option>
                              <option value="windows.jpg">Colline</option>
                              <option value="sea.jpg">Océan</option>
                         </select>
                    </div>

                    <div className="form-group">
                         <label htmlFor="sizeMultiplier">Taille des icones</label>
                         <select name="sizeMultiplier" value={values.sizeMultiplier} onChange={(e) => handleSizeMultiplierChange(e.target.value)}>
                              <option value="0.75">75%</option>
                              <option value="1">100%</option>
                              <option value="1.25">125%</option>
                         </select>
                    </div>
               </div>
          </Window>
     )
}
