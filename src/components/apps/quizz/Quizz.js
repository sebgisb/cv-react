import React, { useEffect, useState } from 'react'
import Window from '../components/window/Window'
import './quizz.scss'
import QuizzForm from './component/quizzform/QuizzForm'
import Questions from './component/questions/Questions'
import Game from './component/game/Game'
export const QuizzContext = React.createContext()

const baseUrl = "https://the-trivia-api.com/api/questions?"

export default function Quizz({ values }) {

     const [questionsNumber, setQuestionsNumber] = useState(5)
     const [category, setCategory] = useState('')
     const [difficulty, setDifficulty] = useState('medium')
     const [hasGameStarded, setHasGameStarted] = useState(false)
     const [url, setUrl] = useState('')

     useEffect(() => {
          values.setIsFullScreen(true)
     }, [])

     const gameStart = () => {
          let questionUrl;
          let cat = category.replaceAll(' ', '_')
          cat = cat.replace('&', 'and')
          questionUrl = baseUrl + "limit=" + questionsNumber + "&categories=" + cat + "&difficulty=" + difficulty
          setUrl(questionUrl)
          setHasGameStarted(true)
     }

     return (
          <Window values={values}>
               <QuizzContext.Provider value="null">
                    <div className="quizz">
                         {!hasGameStarded ?
                              (<>
                                   <h1>Quizz</h1>

                                   <QuizzForm
                                        gameStart={gameStart}
                                        category={category}
                                        setCategory={setCategory}
                                        questionsNumber={questionsNumber}
                                        setQuestionsNumber={setQuestionsNumber}
                                        difficulty={difficulty}
                                        setDifficulty={setDifficulty}
                                   />
                              </>)
                              :
                              (<Game url={url} setHasGameStarted={setHasGameStarted} />)
                         }
                    </div>
               </QuizzContext.Provider>

          </Window>
     )
}
