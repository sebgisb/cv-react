import React, { useState, useEffect } from 'react'
import useFetch from '../../../../../hook/useFetch'
import Results from '../results/Results';
import Questions from '../questions/Questions';


// const questions = [
//      {
//           "category": "General Knowledge",
//           "id": "622a1c3a7cc59eab6f951030",
//           "correctAnswer": "Northern Crown",
//           "incorrectAnswers": [
//                "Brave Hunter",
//                "Dazzling Forest",
//                "Great Light",
//                "Northern Crown"
//           ],
//           "question": "What is the meaning of the name of the constellation Corona Borealis?",
//           "tags": [
//                "general_knowledge"
//           ],
//           "type": "Multiple Choice",
//           "difficulty": "medium",
//           "regions": []
//      },
//      {
//           "category": "General Knowledge",
//           "id": "622a1c357cc59eab6f94fc7d",
//           "correctAnswer": "Gibberish",
//           "incorrectAnswers": [
//                "Gobbledygook",
//                "Blatherskite",
//                "Flummox"
//           ],
//           "question": "Which word is defined as 'unintelligible or meaningless speech'?",
//           "tags": [
//                "words",
//                "general_knowledge"
//           ],
//           "type": "Multiple Choice",
//           "difficulty": "medium",
//           "regions": []
//      },
//      {
//           "category": "General Knowledge",
//           "id": "622a1c367cc59eab6f9503a2",
//           "correctAnswer": "Harvard",
//           "incorrectAnswers": [
//                "Yale",
//                "Cornell",
//                "Princeton"
//           ],
//           "question": "Which University, founded in 1636, is the oldest in the USA?",
//           "tags": [
//                "general_knowledge"
//           ],
//           "type": "Multiple Choice",
//           "difficulty": "medium",
//           "regions": []
//      },
//      {
//           "category": "General Knowledge",
//           "id": "6239f81e9129041bf3925cc8",
//           "correctAnswer": "Lima",
//           "incorrectAnswers": [
//                "Lame",
//                "Loan",
//                "Love"
//           ],
//           "question": "What word is used in the NATO Phonetic Alphabet for the letter L?",
//           "tags": [
//                "general_knowledge"
//           ],
//           "type": "Multiple Choice",
//           "difficulty": "medium",
//           "regions": []
//      },
//      {
//           "category": "General Knowledge",
//           "id": "6239f8289129041bf3925ccc",
//           "correctAnswer": "Papa",
//           "incorrectAnswers": [
//                "Pear",
//                "Peer",
//                "Peeler"
//           ],
//           "question": "What word is used in the NATO Phonetic Alphabet for the letter P?",
//           "tags": [
//                "general_knowledge"
//           ],
//           "type": "Multiple Choice",
//           "difficulty": "medium",
//           "regions": []
//      }
// ]
export default function Game({ url, setHasGameStarted }) {

     const [questions, pending, error] = useFetch(url)
     const [resultsArray, setResultsArray] = useState([]);
     const [isGameOver, setIsGameOver] = useState(false)

     useEffect(() => {
          if (questions) {
               console.log(questions)
               if (resultsArray.length === questions.length) {
                    setIsGameOver(true)
               }
          }
     }, [resultsArray])

     return (<>
          {questions &&
               <div>
                    {!isGameOver ?
                         (<Questions
                              questions={questions}
                              resultsArray={resultsArray}
                              setResultsArray={setResultsArray}
                              setIsGameOver={setIsGameOver}
                         />)
                         :
                         (<Results
                              questions={questions} resultsArray={resultsArray} setHasGameStarted={setHasGameStarted} />
                         )
                    }

               </div>
          }
     </>
     )
}
