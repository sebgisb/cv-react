import React from 'react'

export default function Possibilities({ possibleAnswers, setGuess }) {

     function handleClick(e, answer) {
          const active = document.querySelector(".active");

          e.target.classList.add("active");
          setGuess(answer);
          if (active || active === e.target) {
               active.classList.remove("active");
               if (active === e.target) {
                    setGuess(null);
               }
          }
     }

     return (
          <div className="possibilities">
               {possibleAnswers.map((answer) => (
                    <div
                         onClick={(e) => handleClick(e, answer)}
                         className="possibilitie"
                         key={answer.key}
                    >
                         {answer}
                    </div>
               ))}
          </div>
     )
}
