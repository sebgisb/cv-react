import React, { useState, useEffect } from 'react'

export default function Progress({ questions, resultsArray }) {



     const [percent, setPercent] = useState(0)

     useEffect(() => {
          if (questions) {
               setPercent(Math.round((100 * resultsArray.length) / questions.length))
          }
     }, [resultsArray])

     return (
          <>
               <div className="quizz-overview">
                    {questions &&
                         questions.map((question, key) =>
                              question.correctAnswer === resultsArray[key] ? (
                                   <div key={key} className="quizz-true quizz-item"> </div>
                              ) : resultsArray[key] === undefined ? (
                                   <div key={key} className="quizz-empty quizz-item"></div>
                              ) : (
                                   <div key={key} className="quizz-false quizz-item"></div>
                              )
                         )}
               </div>
               <div className="progress">{percent} %</div>
          </>
     )
}
