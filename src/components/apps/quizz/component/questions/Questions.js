import React, { useState, useEffect } from 'react'
import useFetch from '../../../../../hook/useFetch'
import Possibilities from './Possibilities';
import Progress from './Progress';

export default function Questions({ questions, resultsArray, setResultsArray }) {

     const [currentQuestion, setCurrentQuestion] = useState(0)
     const [possibleAnswers, setPossibleAnswers] = useState([]);
     const [guess, setGuess] = useState(null);
     const [goodAnswer, setGoodAnswer] = useState();

     useEffect(() => {
          let possibleAnswersArray = questions[currentQuestion].incorrectAnswers;
          possibleAnswersArray.push(questions[currentQuestion].correctAnswer);

          setPossibleAnswers(() => possibleAnswersArray.sort());

          setGoodAnswer(questions[currentQuestion].correctAnswer);
     }, [questions, currentQuestion])

     function handleSubmit() {
          if (guess !== null) {
               setResultsArray([...resultsArray, guess]);

               if (currentQuestion + 1 === questions.length) {
                    return;
               }
               setCurrentQuestion(currentQuestion + 1);
               setGuess(null);
          }
     }

     return (
          <div className="wrapper">

               <div className="question-header">
                    <h2>Question {currentQuestion + 1} :</h2>
                    <p className="question">{questions[currentQuestion].question}</p>
               </div>

               <Possibilities setGuess={setGuess} possibleAnswers={possibleAnswers} />

               <Progress questions={questions} resultsArray={resultsArray} />

               <div onClick={() => handleSubmit()} className="btn">
                    Valider
               </div>

          </div>
     )
}
