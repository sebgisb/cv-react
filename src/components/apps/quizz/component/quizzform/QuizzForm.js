import React, { useContext, useEffect } from 'react'

import useFetch from '../../../../../hook/useFetch'
import QuizzContext from '../../Quizz'
import Modal from '../../../components/modal/Modal'

export default function QuizzForm(props) {

     const { gameStart, category, setCategory, questionsNumber, setQuestionsNumber, difficulty, setDifficulty } = props

     const [categories, pending, error] = useFetch('https://the-trivia-api.com/api/categories')


     return (
          <Modal trigger="Nouvelle partie" gameStart={gameStart}>

               <div className="quizz-form">
                    <div className="form-group">
                         <label htmlFor="category">Catégorie :</label>
                         <select name="category" value={category} onChange={(e) => setCategory(e.target.value)}>
                              <option value="" >Toutes</option>
                              {categories && Object.keys(categories).map((cat, idx) => {
                                   return <option key={idx} value={cat}>{cat}</option>
                              })}
                         </select>
                    </div>

                    <div className="form-group">
                         <label htmlFor="questionsNumber">Nombre de questions : {questionsNumber}</label>
                         <input onChange={(e) => setQuestionsNumber(e.target.value)} value={questionsNumber} type="range" min="1" max="30" step="1" name="questionsNumber" />
                    </div>
                    <div className="form-group">

                         <label htmlFor="difficulty">Difficulté :</label>
                         <select value={difficulty} onChange={(e) => setDifficulty(e.target.value)} name="difficulty">
                              <option value="easy">Easy</option>
                              <option value="medium">Medium</option>
                              <option value="hard">Hard</option>
                         </select>
                    </div>

               </div>
          </Modal>
     )
}
