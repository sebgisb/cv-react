import React, { useEffect, useState, useRef } from 'react'
export default function Results({ questions, resultsArray, setHasGameStarted }) {


     const [percent, setPercent] = useState(0);


     useEffect(() => {
          calcResults()
     }, [])

     function calcResults() {
          let results = 0;
          for (let i = 0; i < questions.length; i++) {

               if (questions[i].correctAnswer == resultsArray[i]) {
                    results++
               }
          }

          setPercent(Math.round((100 / questions.length) * results) + '%')

     }



     return (
          <div className="results">
               <div className="backToMenu" onClick={() => setHasGameStarted(false)}>
                    <p>Menu</p>
                    <img className="arrow-back" src={'icones/curved-arrow.png'} />
               </div>
               <div className="results-graph">
                    <p>{percent}</p>
                    <div style={{ height: percent }}> </div>
               </div>

               <div className="questions-results">
                    {questions.map((question, key) => {
                         if (resultsArray[key] == question.correctAnswer) {
                              return (
                                   <div key={key} className="question">
                                        <h5><span style={{ backgroundColor: 'green' }}></span> Question {key + 1} :</h5>
                                        <h3> {question.question}</h3>
                                        <h4>Answer : {question.correctAnswer}</h4>
                                   </div>

                              )
                         } else {

                              return (<div key={key} className="question">

                                   <h5> <span style={{ backgroundColor: 'red' }}></span>Question {key + 1} :</h5>
                                   <h3> {question.question}</h3>
                                   <h4>Answer : {question.correctAnswer}</h4>
                              </div>
                              )
                         }
                    })}
               </div>

          </div >
     )
}
