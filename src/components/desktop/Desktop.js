import React, { useContext, useEffect } from 'react'
import './desktop.scss'
import DesktopShortcuts from './components/DesktopShortcuts'
import DesktopGrid from './components/DesktopGrid'

export default function Desktop({ appsValues, children }) {

     return (
          <div className="desktop" style={{ backgroundImage: 'url(images/' + appsValues.params.background + ')' }}>

               <DesktopGrid >


               </DesktopGrid>
               {Object.values(appsValues).map((app, idx) => {
                    return (
                         <DesktopShortcuts key={idx}
                              isOpen={app.isOpen}
                              setIsOpen={app.setIsOpen}
                              setIsMinimized={app.setIsMinimized}
                              icone={app.icone}
                              title={app.title}
                         />
                    )
               })}
               {children}
          </div>
     )
}
