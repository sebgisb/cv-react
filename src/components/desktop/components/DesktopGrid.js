import React, { useRef, useEffect } from 'react'

export default function DesktopGrid({ children }) {

     const gridRef = useRef()

     const clamp = (num, min, max) => Math.min(Math.max(num, min), max);

     useEffect(() => {
          window.addEventListener('resize', handleResize)
          generateGrid()

     }, [])

     function generateGrid() {

          let iconePositionArray = document.querySelectorAll('.desktop-shortcut')
          gridRef.current.innerHTML = ''
          let gridWidth = gridRef.current.offsetWidth
          let gridHeight = gridRef.current.offsetHeight
          let cellsArray = []

          let cellWidth = clamp(gridWidth / 3, 80, 120);
          let cellHeight = clamp(gridHeight / 5, gridHeight / 4, 100);
          let rowCount = Math.floor(gridHeight / cellHeight)
          let columnsCount = Math.floor(gridWidth / cellWidth)


          for (let r = 0; r < rowCount; r++) {
               let row = document.createElement('div')
               row.classList.add('row')
               gridRef.current.appendChild(row)

               for (let c = 0; c < columnsCount; c++) {
                    let cell = document.createElement('div');
                    cell.classList.add('cell')
                    cell.setAttribute('data-col', c)
                    cell.setAttribute('data-row', r)
                    cell.setAttribute('droppable', true)
                    row.appendChild(cell)
                    cell.style.height = cellHeight + 'px'
                    cell.style.width = cellWidth + 'px'
                    cell.addEventListener('dragover', dragOver)
                    cell.addEventListener('drop', drop)
                    cell.addEventListener('dragenter', dragLeave)
                    cellsArray.push(cell)
               }
          }

          for (let i = 0; i < iconePositionArray.length; i++) {

               iconePositionArray[i].addEventListener('dragstart', dragStart)

               let parent = document.querySelector('[data-col = "' + iconePositionArray[i].getAttribute('data-col') + '"][data-row = "' + iconePositionArray[i].getAttribute('data-row') + '"].cell')
               if (parent) {
                    parent.appendChild(iconePositionArray[i])
                    parent.removeAttribute('droppable')
                    iconePositionArray[i].setAttribute('data-col', parent.getAttribute('data-col'))
                    iconePositionArray[i].setAttribute('data-row', parent.getAttribute('data-row'))
               } else {

                    for (let e = 0; e < cellsArray.length; e++) {
                         if (cellsArray[e].getAttribute('droppable')) {
                              iconePositionArray[i].setAttribute('data-col', cellsArray[e].getAttribute('data-col'))
                              iconePositionArray[i].setAttribute('data-row', cellsArray[e].getAttribute('data-row'))
                              cellsArray[e].appendChild(iconePositionArray[i])
                              cellsArray[e].removeAttribute('droppable')
                              break
                         }
                    }
               }
          }


     }

     let timeOut
     function handleResize() {

          clearTimeout(timeOut)

          timeOut = setTimeout(generateGrid, 700);
     }

     let currentBox = '';

     function dragStart(e) {
          e.target.classList.add('dragged')
          currentBox = e.target.parentNode

     }

     function dragOver(e) {
          e.preventDefault();
     }

     function dragLeave(e) {

     }

     function drop(e) {

          const dragged = document.querySelector('.dragged')
          dragged.classList.remove('dragged')

          if (!e.target.getAttribute('droppable') === true) {
               return
          }

          dragged.setAttribute('data-col', e.target.getAttribute('data-col'))
          dragged.setAttribute('data-row', e.target.getAttribute('data-row'))
          currentBox.setAttribute('droppable', true)
          e.target.appendChild(dragged);
          e.target.removeAttribute('droppable')
     }


     return (
          <div ref={gridRef} className="desktop-grid">{children}</div>
     )
}
