import React from 'react'

export default function DesktopShortcuts({ setIsOpen, icone, title, isOpen, setIsMinimized }) {

     const handleClick = event => {


          if (event.detail === 2) {
               if (isOpen) {
                    setIsMinimized(false)
               } else {
                    setIsOpen(true)
               }
          }
     };

     return (
          <div className={'desktop-shortcut ' + title} draggable="true" onClick={handleClick}>
               <img src={'icones/' + icone} alt={'Raccourci'} />
               <p>{title}</p>
          </div>
     )
}
