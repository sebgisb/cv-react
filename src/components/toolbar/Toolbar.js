import React from 'react'
import './toolbar.scss'
import Menu from './components/menu/Menu'
import Meteo from './components/meteo/Meteo'
import ActiveBarIcone from './components/activebar/ActiveBarIcone'
import FullScreen from './components/fullscreen/Fullscreen'

import Clock from './components/horloge/Clock'

export default function Toolbar({ appsValues }) {

     return (
          <div className="toolbar">
               <Menu appsValues={appsValues} />

               <div className="active-bar">
                    {Object.values(appsValues).map((app, idx) => {

                         if (app.isOpen) {
                              return (
                                   <ActiveBarIcone
                                        key={idx}
                                        isMinimized={app.isMinimized}
                                        setIsMinimized={app.setIsMinimized}
                                        title={app.title}
                                        icone={app.icone}
                                   />
                              )
                         }
                    })}
               </div>
               <div className="widgets-container">
                    <FullScreen />
                    <Meteo />
                    <Clock />
               </div>
          </div>
     )
}
