import React from 'react'

export default function ActiveBarIcone({ icone, isMinimized, setIsMinimized }) {
     return (
          <div className="active-bar-icone" onClick={() => setIsMinimized(!isMinimized)}>
               <img src={'icones/' + icone} alt={'Raccourci'} />
          </div>
     )
}
