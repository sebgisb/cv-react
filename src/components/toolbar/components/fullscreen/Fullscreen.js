import React, { useState } from "react";

export default function FullScreen() {



     function openFullscreen() {
          let app = document.querySelector(".app");

          if (document.fullscreenElement) {
               document.exitFullscreen();
          }
          app.requestFullscreen();
     }

     return (
          <div onClick={openFullscreen} className="app-fullscreen widget">
               <img src="icones/expand.svg" alt="Plein écran" />
          </div>
     );
}
