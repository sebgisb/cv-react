import React, { useEffect, useState } from 'react'

export default function Clock() {

     const [dateTime, setDateTime] = useState(new Date());

     useEffect(() => {
          setInterval(() => setDateTime(new Date()), 1000);
     }, []);

     return (
          <div className="clock widget">
               <div>
                    <p>
                         {dateTime.toLocaleTimeString([], {
                              hour: "2-digit",
                              minute: "2-digit",
                         })}
                    </p>

                    <p>
                         {dateTime.toLocaleDateString()}
                    </p>
               </div>

          </div>
     );
}
