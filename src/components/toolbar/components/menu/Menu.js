import React, { useState } from 'react'
import Panel from './components/Panel'
import './toolbarmenu.scss'
export default function Menu({ appsValues }) {

     const [isPanelOpen, setIsPanelOpen] = useState(false)

     return (
          <div className="toolbar-menu widget" onClick={() => setIsPanelOpen(!isPanelOpen)}>
               <img src="icones/windows.svg" />

               {isPanelOpen && <Panel appsValues={appsValues} setIsPanelOpen={setIsPanelOpen} />}
          </div>
     )
}
