import React, { useEffect, useRef } from 'react'

export default function Panel({ appsValues, setIsPanelOpen }) {
     const panel = useRef()
     useEffect(() => { console.log(appsValues) }, [appsValues])

     useEffect(() => {
          document.querySelector('.desktop ').addEventListener('click', closePanel)
          return () => { document.querySelector('.desktop ').removeEventListener('click', closePanel) }
     }, [])

     function closePanel(evt) {
          if (!panel.current.contains(evt.target)) {
               setIsPanelOpen(false)
          }
     }

     return (
          <div ref={panel} className="panel">
               {Object.values(appsValues).map((app, idx) => {
                    return <div key={idx} className="items" onClick={() => app.setIsOpen(true)}> <img src={'icones/' + app.icone} /><p>{app.title}</p></div>
               })}

          </div>
     )
}
