import React, { useState, useEffect } from 'react'
import './meteo.scss'
import Widget from './components/widget/Widget'
import Panel from './components/panel/Panel'
// const ersatz = {
//      "lat": 48.8566,
//      "lon": 2.3522,
//      "timezone": "Europe/Paris",
//      "timezone_offset": 7200,
//      "current": {
//           "dt": 1662811490,
//           "sunrise": 1662787195,
//           "sunset": 1662833743,
//           "temp": 17.55,
//           "feels_like": 17.71,
//           "pressure": 1017,
//           "humidity": 90,
//           "dew_point": 15.89,
//           "uvi": 3.8,
//           "clouds": 100,
//           "visibility": 10000,
//           "wind_speed": 4.63,
//           "wind_deg": 270,
//           "weather": [
//                {
//                     "id": 804,
//                     "main": "Clouds",
//                     "description": "couvert",
//                     "icon": "04d"
//                }
//           ]
//      },
//      "daily": [
//           {
//                "dt": 1662807600,
//                "sunrise": 1662787195,
//                "sunset": 1662833743,
//                "moonrise": 1662835320,
//                "moonset": 1662785760,
//                "moon_phase": 0.5,
//                "temp": {
//                     "day": 17.69,
//                     "min": 14.64,
//                     "max": 20.01,
//                     "night": 16.76,
//                     "eve": 20.01,
//                     "morn": 14.64
//                },
//                "feels_like": {
//                     "day": 17.76,
//                     "night": 16.53,
//                     "eve": 19.76,
//                     "morn": 14.41
//                },
//                "pressure": 1017,
//                "humidity": 86,
//                "dew_point": 15.32,
//                "wind_speed": 4.62,
//                "wind_deg": 244,
//                "wind_gust": 11.48,
//                "weather": [
//                     {
//                          "id": 500,
//                          "main": "Rain",
//                          "description": "légère pluie",
//                          "icon": "10d"
//                     }
//                ],
//                "clouds": 100,
//                "pop": 0.85,
//                "rain": 0.77,
//                "uvi": 3.8
//           },
//           {
//                "dt": 1662894000,
//                "sunrise": 1662873679,
//                "sunset": 1662920016,
//                "moonrise": 1662922740,
//                "moonset": 1662877020,
//                "moon_phase": 0.54,
//                "temp": {
//                     "day": 22.38,
//                     "min": 14.24,
//                     "max": 25.12,
//                     "night": 20,
//                     "eve": 24.21,
//                     "morn": 14.24
//                },
//                "feels_like": {
//                     "day": 21.93,
//                     "night": 19.47,
//                     "eve": 23.7,
//                     "morn": 13.97
//                },
//                "pressure": 1019,
//                "humidity": 48,
//                "dew_point": 10.65,
//                "wind_speed": 2.78,
//                "wind_deg": 102,
//                "wind_gust": 7.64,
//                "weather": [
//                     {
//                          "id": 800,
//                          "main": "Clear",
//                          "description": "ciel dégagé",
//                          "icon": "01d"
//                     }
//                ],
//                "clouds": 9,
//                "pop": 0,
//                "uvi": 4.64
//           },
//           {
//                "dt": 1662980400,
//                "sunrise": 1662960163,
//                "sunset": 1663006288,
//                "moonrise": 1663010160,
//                "moonset": 1662968220,
//                "moon_phase": 0.57,
//                "temp": {
//                     "day": 25.76,
//                     "min": 16.17,
//                     "max": 29.24,
//                     "night": 24.11,
//                     "eve": 27.66,
//                     "morn": 16.17
//                },
//                "feels_like": {
//                     "day": 25.28,
//                     "night": 23.46,
//                     "eve": 26.73,
//                     "morn": 15.75
//                },
//                "pressure": 1011,
//                "humidity": 34,
//                "dew_point": 8.58,
//                "wind_speed": 3.38,
//                "wind_deg": 114,
//                "wind_gust": 8.87,
//                "weather": [
//                     {
//                          "id": 802,
//                          "main": "Clouds",
//                          "description": "partiellement nuageux",
//                          "icon": "03d"
//                     }
//                ],
//                "clouds": 41,
//                "pop": 0,
//                "uvi": 4.73
//           },
//           {
//                "dt": 1663066800,
//                "sunrise": 1663046648,
//                "sunset": 1663092560,
//                "moonrise": 1663097580,
//                "moonset": 1663059180,
//                "moon_phase": 0.61,
//                "temp": {
//                     "day": 24.84,
//                     "min": 20.44,
//                     "max": 28.65,
//                     "night": 20.44,
//                     "eve": 24.03,
//                     "morn": 22.16
//                },
//                "feels_like": {
//                     "day": 24.76,
//                     "night": 20.58,
//                     "eve": 24.16,
//                     "morn": 21.66
//                },
//                "pressure": 1006,
//                "humidity": 53,
//                "dew_point": 14.3,
//                "wind_speed": 4.84,
//                "wind_deg": 246,
//                "wind_gust": 9.06,
//                "weather": [
//                     {
//                          "id": 500,
//                          "main": "Rain",
//                          "description": "légère pluie",
//                          "icon": "10d"
//                     }
//                ],
//                "clouds": 100,
//                "pop": 0.55,
//                "rain": 1.33,
//                "uvi": 3.8
//           },
//           {
//                "dt": 1663153200,
//                "sunrise": 1663133132,
//                "sunset": 1663178832,
//                "moonrise": 1663185120,
//                "moonset": 1663150080,
//                "moon_phase": 0.64,
//                "temp": {
//                     "day": 25.81,
//                     "min": 18.36,
//                     "max": 26.15,
//                     "night": 19.2,
//                     "eve": 21.75,
//                     "morn": 18.36
//                },
//                "feels_like": {
//                     "day": 25.78,
//                     "night": 19.47,
//                     "eve": 21.99,
//                     "morn": 18.58
//                },
//                "pressure": 1005,
//                "humidity": 51,
//                "dew_point": 14.71,
//                "wind_speed": 4.21,
//                "wind_deg": 274,
//                "wind_gust": 10.06,
//                "weather": [
//                     {
//                          "id": 501,
//                          "main": "Rain",
//                          "description": "pluie modérée",
//                          "icon": "10d"
//                     }
//                ],
//                "clouds": 64,
//                "pop": 0.93,
//                "rain": 9.66,
//                "uvi": 4.16
//           },
//           {
//                "dt": 1663239600,
//                "sunrise": 1663219616,
//                "sunset": 1663265104,
//                "moonrise": 1663272900,
//                "moonset": 1663240920,
//                "moon_phase": 0.67,
//                "temp": {
//                     "day": 21.87,
//                     "min": 16.84,
//                     "max": 23.25,
//                     "night": 18.89,
//                     "eve": 20.85,
//                     "morn": 16.84
//                },
//                "feels_like": {
//                     "day": 21.26,
//                     "night": 18.35,
//                     "eve": 20.32,
//                     "morn": 16.77
//                },
//                "pressure": 1014,
//                "humidity": 44,
//                "dew_point": 8.93,
//                "wind_speed": 3.07,
//                "wind_deg": 12,
//                "wind_gust": 5.27,
//                "weather": [
//                     {
//                          "id": 500,
//                          "main": "Rain",
//                          "description": "légère pluie",
//                          "icon": "10d"
//                     }
//                ],
//                "clouds": 86,
//                "pop": 0.89,
//                "rain": 1.39,
//                "uvi": 5
//           },
//           {
//                "dt": 1663326000,
//                "sunrise": 1663306101,
//                "sunset": 1663351375,
//                "moonrise": 1663361040,
//                "moonset": 1663331580,
//                "moon_phase": 0.7,
//                "temp": {
//                     "day": 18.47,
//                     "min": 16.57,
//                     "max": 20.26,
//                     "night": 17.67,
//                     "eve": 18.55,
//                     "morn": 16.57
//                },
//                "feels_like": {
//                     "day": 17.78,
//                     "night": 16.93,
//                     "eve": 17.84,
//                     "morn": 15.98
//                },
//                "pressure": 1016,
//                "humidity": 54,
//                "dew_point": 8.83,
//                "wind_speed": 2.76,
//                "wind_deg": 54,
//                "wind_gust": 3.88,
//                "weather": [
//                     {
//                          "id": 804,
//                          "main": "Clouds",
//                          "description": "couvert",
//                          "icon": "04d"
//                     }
//                ],
//                "clouds": 100,
//                "pop": 0,
//                "uvi": 5
//           },
//           {
//                "dt": 1663412400,
//                "sunrise": 1663392585,
//                "sunset": 1663437646,
//                "moonrise": 1663449660,
//                "moonset": 1663422000,
//                "moon_phase": 0.75,
//                "temp": {
//                     "day": 21.22,
//                     "min": 14.31,
//                     "max": 22.38,
//                     "night": 16.39,
//                     "eve": 19.93,
//                     "morn": 14.31
//                },
//                "feels_like": {
//                     "day": 20.44,
//                     "night": 15.73,
//                     "eve": 19.18,
//                     "morn": 13.55
//                },
//                "pressure": 1014,
//                "humidity": 40,
//                "dew_point": 6.9,
//                "wind_speed": 3.88,
//                "wind_deg": 356,
//                "wind_gust": 6.15,
//                "weather": [
//                     {
//                          "id": 801,
//                          "main": "Clouds",
//                          "description": "peu nuageux",
//                          "icon": "02d"
//                     }
//                ],
//                "clouds": 24,
//                "pop": 0,
//                "uvi": 5
//           }
//      ]
// }

const key = "015e2429afb8249ad402711bbf70ad0b"
const exclude = "minutely,hourly,alerts"
const lang = "fr"
const units = "metric"
let lat;
let lon;
let url;

export default function Meteo() {

     const [weather, setWeather] = useState()
     const [isPanelOpen, setIsPanelOpen] = useState(false)


     useEffect(() => {
          if (navigator.geolocation) {
               navigator.geolocation.getCurrentPosition(function (position) {
                    lat = position.coords.latitude
                    lon = position.coords.longitude;
                    url = `https://api.openweathermap.org/data/3.0/onecall?lat=${lat}&lon=${lon}&units=${units}&exclude=${exclude}&lang=${lang}&appid=${key}`

                    fetchWeather();
               });
          }
     }, [])

     async function fetchWeather() {
          try {
               console.log(url)
               const response = await fetch(url)
               const data = await response.json();
               setWeather(data);
          } catch (err) {
               console.log(err);
          }
     }


     function setSetIsPanelOpen(dd) {
          setIsPanelOpen(dd)
     }


     return (
          <div className="meteo">
               {weather && <>
                    <Widget
                         setIsPanelOpen={setSetIsPanelOpen}
                         isPanelOpen={isPanelOpen}
                         icon={weather.current.weather[0].icon}
                         temp={weather.current.temp + '°C'}
                         description={weather.current.weather[0].description}
                    />
                    {
                         isPanelOpen &&
                         <Panel weather={weather} setIsPanelOpen={setIsPanelOpen} />
                    }</>}
          </div>
     )


}
