import React, { useEffect, useRef, useState } from 'react'
import Card from './components/Card'


export default function Panel({ weather, setIsPanelOpen }) {

     const ref = useRef()
     const panel = useRef()
     const [currentChild, setCurrentChild] = useState(0)
     const Arrow = 'arrow-next.png'

     useEffect(() => {
          document.querySelector('.desktop ').addEventListener('click', closePanel)
          return () => { document.querySelector('.desktop ').removeEventListener('click', closePanel) }
     }, [])

     useEffect(() => {
          ref.current.style.transform = "translate( -" + currentChild * ref.current.childNodes[currentChild].getBoundingClientRect().width + "px)";
     }, [currentChild])


     function closePanel(evt) {
          if (!panel.current.contains(evt.target)) {
               setIsPanelOpen(false)
          }
     }

     function nextItem() {
          if (currentChild === weather.daily.length - 1) {
               setCurrentChild(0)
          } else {
               setCurrentChild(currentChild + 1)
          }
     }

     function prevItem() {
          if (currentChild === 0) {
               setCurrentChild(weather.daily.length - 1)
          } else {
               setCurrentChild(currentChild - 1)
          }
     }

     return (
          <div ref={panel} className="panel">
               <div ref={ref} className="meteo-card-container">
                    {
                         weather.daily.map((day, idx) => {
                              return (
                                   <div key={idx} className="meteo-card-group">
                                        {idx > 0 ? <div onClick={prevItem} className="carroussel-prev carroussel-control"><img src={'icones/' + Arrow} alt="précédent" /></div> : ''}

                                        <div onClick={nextItem} className="carroussel-next carroussel-control"><img src={'icones/' + Arrow} alt="suivant" /></div>
                                        <Card weather={day} />
                                   </div>
                              )
                         })
                    }
               </div>

          </div>
     )
}
