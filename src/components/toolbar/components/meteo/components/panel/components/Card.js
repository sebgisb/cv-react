import React, { useEffect, useState } from 'react'

export default function Card({ weather }) {

     const [date, setDate] = useState()

     useEffect(() => {
          setDate(new Date(weather.dt * 1000))
     }, [])

     const dateFormat = { day: 'numeric', weekday: 'long', month: 'long' }

     return (
          <div className="meteo-card">
               <div className="card-header">
                    <h4>{date && date.toLocaleDateString('fr-FR', dateFormat)}</h4>
                    <img src={`http://openweathermap.org/img/wn/${weather.weather[0].icon}@4x.png`} />
                    <h3><p>{weather.weather[0].description}</p></h3>
               </div>
               <div className="card-body">
                    <ul>
                         <li>Min: {parseInt(weather.temp.min)}°C</li>
                         <li>Max: {parseInt(weather.temp.max)}°C</li>
                         <li>Vent: {weather.wind_speed} km/h</li>
                         <li>Rafale: {weather.wind_gust} km/h</li>
                         <li>Humidité: {weather.humidity}%</li>
                         {
                              <li>Précipitations: {weather.rain > 0 ? weather.rain : 0}cm</li>
                         }
                    </ul>
               </div>


          </div>
     )
}
