import React, { useEffect } from 'react'
export default function Widget({ icon, description, temp, setIsPanelOpen, isPanelOpen }) {
     useEffect(() => {

     }, [temp])
     return (
          <div className="widget" onClick={() => setIsPanelOpen(!isPanelOpen)}>
               <img className="widget-icon" src={`http://openweathermap.org/img/wn/${icon}.png`} alt={description} />
               <p>{parseInt(temp) + '°C'}</p>
               <p>{description}</p>
          </div>
     )
}
