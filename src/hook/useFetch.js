import React, { useEffect, useState } from 'react';

export default function useFetch(url) {
     const [error, setError] = useState(null);
     const [data, setData] = useState(null);
     const [pending, setPending] = useState(false);
     useEffect(() => {
          async function doFetch() {
               setPending(true);
               try {
                    const response = await fetch(url);
                    setData(await response.json());
               } catch (err) {
                    setError(err);
               } finally {
                    setPending(false);
               }
          }

          if (url && !pending) {
               doFetch();
          }
     }, [url]);

     return [data, pending, error];
}